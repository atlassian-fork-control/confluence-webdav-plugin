package com.atlassian.confluence.extra.webdav.job;

import com.atlassian.sal.api.transaction.TransactionTemplate;
import junit.framework.TestCase;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ContentJobQueueTestCase extends TestCase {
    @Mock
    private TransactionTemplate transactionTemplate;

    private ContentJobQueue contentJobQueue;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        MockitoAnnotations.initMocks(this);
        contentJobQueue = new ContentJobQueue(transactionTemplate);
    }

    @Override
    protected void tearDown() throws Exception {
        transactionTemplate = null;
        super.tearDown();
    }

    public void testJobExecutedWhenTimedOut() throws Exception {
        ContentJob contentJob = mock(ContentJob.class);

        when(contentJob.getMinimumAgeForExecution()).thenReturn(-1L);

        contentJobQueue.enque(contentJob);
        contentJobQueue.executeTasks();

        verify(transactionTemplate).execute(any(ContentJobQueueTransactionCallback.class));
    }

    public void testJobNotExecutedIfNotTimedOut() throws Exception {
        ContentJob contentJob = mock(ContentJob.class);

        when(contentJob.getMinimumAgeForExecution()).thenReturn(Long.MAX_VALUE);

        contentJobQueue.enque(contentJob);
        contentJobQueue.executeTasks();

        verify(transactionTemplate, never()).execute(any(ContentJobQueueTransactionCallback.class));
    }

    public void testEnqueudJobsCancelledIfEqualsToTheOneBeingRemoved() throws Exception {
        ContentJob contentJob = mock(ContentJob.class);
        ContentJob contentJob2 = mock(ContentJob.class);

        when(contentJob.getMinimumAgeForExecution()).thenReturn(-1L);
        when(contentJob2.getMinimumAgeForExecution()).thenReturn(-1L);

        contentJobQueue.enque(contentJob);
        contentJobQueue.enque(contentJob2);

        contentJobQueue.remove(contentJob2);

        contentJobQueue.executeTasks();

        verify(transactionTemplate, atMost(1)).execute(any(ContentJobQueueTransactionCallback.class));
    }
}
