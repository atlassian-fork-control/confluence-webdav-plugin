package com.atlassian.confluence.extra.webdav.resource;

import org.apache.jackrabbit.webdav.DavResource;
import org.apache.jackrabbit.webdav.DavResourceIterator;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DashboardResourceImplTestCase extends AbstractConfluenceResourceTestCase {

    private DashboardResourceImpl dashboardResourceImpl;

    @Before
    public void initialise() {
        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix,
                workspacePath,
                workspacePath
        );

        dashboardResourceImpl = new DashboardResourceImpl(
                davResourceLocator, davResourceFactory, lockManager, davSession,
                workspaceName
        );
    }

    @Test
    public void testGetMembersReturnsPersonalAndGlobalResources() {
        boolean hasPersonalResource = false;
        boolean hasGlobalResource = false;

        DavResourceIterator davResourceIterator = dashboardResourceImpl.getMembers();

        assertEquals(2, davResourceIterator.size());

        for (; davResourceIterator.hasNext(); ) {
            DavResource davResource = davResourceIterator.nextResource();

            if (!hasPersonalResource)
                hasPersonalResource = davResource instanceof PersonalSpacesResourceImpl;

            if (!hasGlobalResource)
                hasGlobalResource = davResource instanceof GlobalSpacesResourceImpl;
        }

        assertTrue(hasPersonalResource && hasGlobalResource);
    }

    @Test
    public void testDisplayNameEqualsToWorkspaceName() {
        assertEquals(workspaceName, dashboardResourceImpl.getDisplayName());
    }

    @Test
    public void testCreationTimeIsZero() {
        assertEquals(0, dashboardResourceImpl.getCreationtTime());
    }
}
