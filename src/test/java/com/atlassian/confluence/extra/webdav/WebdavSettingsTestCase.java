package com.atlassian.confluence.extra.webdav;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

public class WebdavSettingsTestCase {

    private WebdavSettings webdavSettings = new WebdavSettings();

    @Test
    public void testSerializable() throws IOException, ClassNotFoundException {
        webdavSettings.setContentExportsResourceEnabled(false);
        webdavSettings.setContentUrlResourceEnabled(false);
        webdavSettings.setContentVersionsResourceEnabled(false);
        webdavSettings.setExcludedClientUserAgentRegexes(singletonList("Test/UserAgent"));

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);

        objectOutputStream.writeObject(webdavSettings);
        objectOutputStream.flush();

        ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(byteArrayOutputStream.toByteArray()));

        WebdavSettings deserializedWebdavSettings = (WebdavSettings) objectInputStream.readObject();

        assertNotSame(webdavSettings, deserializedWebdavSettings);
        assertEquals(webdavSettings, deserializedWebdavSettings);
    }
}
