package com.atlassian.confluence.extra.webdav.job.impl;

import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import junit.framework.TestCase;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AttachmentRemovalJobTestCase extends TestCase {

    private AttachmentRemovalJob attachmentRemovalJob;

    @Mock
    private PageManager pageManager;

    @Mock
    private AttachmentManager attachmentManager;

    private Page page;

    private Attachment attachment;

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        MockitoAnnotations.initMocks(this);

        attachmentRemovalJob = new AttachmentRemovalJob(pageManager, attachmentManager, 0, "file.txt");

        page = new Page();
        attachment = new Attachment();
    }

    @Override
    protected void tearDown() throws Exception {
        pageManager = null;
        attachmentManager = null;
        super.tearDown();
    }

    public void testJobTimesOutAfterFifteenSeconds() {
        assertEquals(15000, attachmentRemovalJob.getMinimumAgeForExecution());
    }

    public void testExecuteRemovesAttachment() throws Exception {
        attachmentRemovalJob.setContainingPageId(1);

        when(pageManager.getPage(attachmentRemovalJob.getContainingPageId())).thenReturn(page);
        when(attachmentManager.getAttachment(page, attachmentRemovalJob.getFileName())).thenReturn(attachment);

        attachmentRemovalJob.execute();

        verify(attachmentManager).removeAttachmentFromServer(attachment);
    }

    public void testExecuteRemovesNoAttachmentIfPageDoesNotExist() throws Exception {
        attachmentRemovalJob.setContainingPageId(1);

        when(pageManager.getPage(attachmentRemovalJob.getContainingPageId())).thenReturn(null);

        attachmentRemovalJob.execute();

        verify(attachmentManager, never()).removeAttachmentFromServer(attachment);
    }

    public void testExecuteRemovesNoAttachmentIfAttachmentDoesNotExist() throws Exception {
        attachmentRemovalJob.setContainingPageId(1);

        when(pageManager.getPage(attachmentRemovalJob.getContainingPageId())).thenReturn(page);
        when(attachmentManager.getAttachment(page, attachmentRemovalJob.getFileName())).thenReturn(null);

        attachmentRemovalJob.execute();

        verify(attachmentManager, never()).removeAttachmentFromServer(attachment);
    }
}
