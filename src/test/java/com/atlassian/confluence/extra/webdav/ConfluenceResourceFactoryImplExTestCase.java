package com.atlassian.confluence.extra.webdav;

import com.atlassian.confluence.core.TimeZone;
import com.atlassian.confluence.extra.webdav.resource.AbstractConfluenceResourceTestCase;
import com.atlassian.confluence.extra.webdav.resource.BlogPostContentResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.BlogPostsDayResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.BlogPostsMonthResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.BlogPostsResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.BlogPostsYearResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.DashboardResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.GeneratedResourceReadMeResource;
import com.atlassian.confluence.extra.webdav.resource.GlobalSpacesResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.NonExistentResource;
import com.atlassian.confluence.extra.webdav.resource.PageAttachmentResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.PageContentResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.PageExportsResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.PagePdfExportContentResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.PageResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.PageUrlResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.PageVersionContentResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.PageVersionsResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.PageWordExportContentResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.PersonalSpacesResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.SpaceAttachmentResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.SpaceContentResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.SpaceResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.WorkspaceResourceImpl;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceDescription;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUserImpl;
import com.atlassian.confluence.user.ConfluenceUserPreferences;
import com.atlassian.user.impl.DefaultUser;
import org.apache.commons.lang.StringUtils;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.apache.jackrabbit.webdav.DavServletRequest;
import org.apache.jackrabbit.webdav.DavServletResponse;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class ConfluenceResourceFactoryImplExTestCase extends AbstractConfluenceResourceTestCase {

    @Mock
    private DavServletRequest davServletRequest;
    @Mock
    private DavServletResponse davServletResponse;
    @Mock
    private HttpSession httpSession;
    @Mock
    private ConfluenceUserPreferences confluenceUserPreferences;

    private Space space;
    private Page page;
    private Attachment attachment;
    private DavResourceFactory confluenceResourceFactory;

    @Before
    public void initialise() {
        user = new ConfluenceUserImpl(new DefaultUser("admin"));
        AuthenticatedUserThreadLocal.set(user);

        space = new Space("ds");

        page = new Page();
        page.setSpace(space);
        page.setTitle("Test");

        attachment = new Attachment();
        attachment.setContainer(page);
        attachment.setFileName("foobar.txt");
        page.addAttachment(attachment);

        when(httpSession.getAttribute(ConfluenceDavSession.class.getName())).thenReturn(davSession);
        when(davServletRequest.getSession()).thenReturn(httpSession);
        when(spaceManager.getSpace(space.getKey())).thenReturn(space);
        when(pageManager.getPage(space.getKey(), page.getTitle())).thenReturn(page);
        when(attachmentManager.getAttachment(page, attachment.getFileName())).thenReturn(attachment);
        when(userAccessor.getConfluenceUserPreferences(user)).thenReturn(confluenceUserPreferences);
        when(confluenceUserPreferences.getTimeZone()).thenReturn(TimeZone.getDefault());
        confluenceResourceFactory = davResourceFactory;
    }

    @Test
    public void testCreateResourceWithPathToWorkspace() throws DavException {
        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix, StringUtils.EMPTY,
                StringUtils.EMPTY
        );

        assertTrue(confluenceResourceFactory.createResource(davResourceLocator, davServletRequest, davServletResponse) instanceof WorkspaceResourceImpl);
    }

    @Test
    public void testCreateResourceWithPathToDashboard() throws DavException {
        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix, workspacePath, workspacePath
        );

        assertTrue(confluenceResourceFactory.createResource(davResourceLocator, davServletRequest, davServletResponse) instanceof DashboardResourceImpl);
    }

    @Test
    public void testCreateResourceWithPathToGlobal() throws DavException {
        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix, workspacePath, workspacePath + "/Global"
        );

        assertTrue(confluenceResourceFactory.createResource(davResourceLocator, davServletRequest, davServletResponse) instanceof GlobalSpacesResourceImpl);
    }

    @Test
    public void testCreateResourceWithPathToPersonal() throws DavException {
        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix, workspacePath, workspacePath + "/Personal"
        );

        assertTrue(confluenceResourceFactory.createResource(davResourceLocator, davServletRequest, davServletResponse) instanceof PersonalSpacesResourceImpl);
    }

    @Test
    public void testCreateResourceWithPathToSpace() throws DavException {
        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix, workspacePath, workspacePath + "/Global/" + space.getKey()
        );

        assertTrue(confluenceResourceFactory.createResource(davResourceLocator, davServletRequest, davServletResponse) instanceof SpaceResourceImpl);
    }

    @Test
    public void testCreateResourceWithPathToSpaceDescription() throws DavException {
        space.setName("Demonstration Space");

        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix, workspacePath, workspacePath + "/Global/" + space.getKey() + "/" + space.getName() + ".txt"
        );

        assertTrue(confluenceResourceFactory.createResource(davResourceLocator, davServletRequest, davServletResponse) instanceof SpaceContentResourceImpl);
    }

    @Test
    public void testCreateResourceWithPathToPage() throws DavException {
        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix, workspacePath, workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle()
        );

        assertTrue(confluenceResourceFactory.createResource(davResourceLocator, davServletRequest, davServletResponse) instanceof PageResourceImpl);
    }

    @Test
    public void testCreateResourceWithPathToChildPage() throws DavException {
        final Page parentPage = new Page();

        parentPage.setTitle("Parent");
        parentPage.addChild(page);

        page = new Page() {
            public List<Page> getAncestors() {
                return singletonList(parentPage);
            }
        };
        page.setTitle("Test");
        page.setParentPage(parentPage);

        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix, workspacePath, workspacePath + "/Global/" + space.getKey() + "/" + parentPage.getTitle() + "/" + page.getTitle()
        );

        assertTrue(confluenceResourceFactory.createResource(davResourceLocator, davServletRequest, davServletResponse) instanceof PageResourceImpl);
    }

    @Test
    public void testCreateResourceWithPathToPageContent() throws DavException {
        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix, workspacePath, workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle() + "/" + page.getTitle() + ".txt"
        );

        assertTrue(confluenceResourceFactory.createResource(davResourceLocator, davServletRequest, davServletResponse) instanceof PageContentResourceImpl);
    }

    @Test
    public void testCreateResourceWithPathToPageUrl() throws DavException {
        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix, workspacePath, workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle() + "/" + page.getTitle() + ".url"
        );

        assertTrue(confluenceResourceFactory.createResource(davResourceLocator, davServletRequest, davServletResponse) instanceof PageUrlResourceImpl);
    }

    @Test
    public void testCreateResourceWithPathToPageAttachment() throws DavException {
        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix, workspacePath, workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle() + "/" + attachment.getFileName()
        );

        assertTrue(confluenceResourceFactory.createResource(davResourceLocator, davServletRequest, davServletResponse) instanceof PageAttachmentResourceImpl);
    }

    @Test
    public void testCreateResourceWithPathToPageVersions() throws DavException {
        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix, workspacePath, workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle() + "/@versions"
        );

        assertTrue(confluenceResourceFactory.createResource(davResourceLocator, davServletRequest, davServletResponse) instanceof PageVersionsResourceImpl);
    }

    @Test
    public void testCreateResourceWithPathToPageVersionText() throws DavException {
        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix, workspacePath, workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle() + "/@versions/Version 1.txt"
        );

        assertTrue(confluenceResourceFactory.createResource(davResourceLocator, davServletRequest, davServletResponse) instanceof PageVersionContentResourceImpl);
    }

    @Test
    public void testCreateResourceWithPathToPageExports() throws DavException {
        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix, workspacePath, workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle() + "/@exports"
        );

        assertTrue(confluenceResourceFactory.createResource(davResourceLocator, davServletRequest, davServletResponse) instanceof PageExportsResourceImpl);
    }

    @Test
    public void testCreateResourceWithPathToPageExportsDocFile() throws DavException {
        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix, workspacePath, workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle() + "/@exports/" + page.getTitle() + ".doc"
        );

        assertTrue(confluenceResourceFactory.createResource(davResourceLocator, davServletRequest, davServletResponse) instanceof PageWordExportContentResourceImpl);
    }

    @Test
    public void testCreateResourceWithPathToPageExportsPdfFile() throws DavException {
        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix, workspacePath, workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle() + "/@exports/" + page.getTitle() + ".pdf"
        );

        assertTrue(confluenceResourceFactory.createResource(davResourceLocator, davServletRequest, davServletResponse) instanceof PagePdfExportContentResourceImpl);
    }

    @Test
    public void testCreateResourceWithPathToSpaceNews() throws DavException {
        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix, workspacePath, workspacePath + "/Global/" + space.getKey() + "/@news"
        );

        assertTrue(confluenceResourceFactory.createResource(davResourceLocator, davServletRequest, davServletResponse) instanceof BlogPostsResourceImpl);
    }

    @Test
    public void testCreateResourceWithPathToSpaceNewsYear() throws DavException {
        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix, workspacePath, workspacePath + "/Global/" + space.getKey() + "/@news/2008"
        );

        assertTrue(confluenceResourceFactory.createResource(davResourceLocator, davServletRequest, davServletResponse) instanceof BlogPostsYearResourceImpl);
    }

    @Test
    public void testCreateResourceWithPathToSpaceNewsYearAndMonth() throws DavException {
        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix, workspacePath, workspacePath + "/Global/" + space.getKey() + "/@news/2008/01"
        );

        assertTrue(confluenceResourceFactory.createResource(davResourceLocator, davServletRequest, davServletResponse) instanceof BlogPostsMonthResourceImpl);
    }

    @Test
    public void testCreateResourceWithPathToSpaceNewsYearAndMonthAndDay() throws DavException {
        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix, workspacePath, workspacePath + "/Global/" + space.getKey() + "/@news/2008/01/01"
        );

        assertTrue(confluenceResourceFactory.createResource(davResourceLocator, davServletRequest, davServletResponse) instanceof BlogPostsDayResourceImpl);
    }

    @Test
    public void testCreateResourceWithPathToSpaceNewsBlog() throws DavException {
        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix, workspacePath, workspacePath + "/Global/" + space.getKey() + "/@news/2008/01/01/blog.txt"
        );

        assertTrue(confluenceResourceFactory.createResource(davResourceLocator, davServletRequest, davServletResponse) instanceof BlogPostContentResourceImpl);
    }

    @Test
    public void testCreateResourceWithPathToSpaceNewPage() throws DavException {
        String nonExistentPageTitle = "New page";

        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix, workspacePath, workspacePath + "/Global/" + space.getKey() + "/" + nonExistentPageTitle
        );

        assertTrue(confluenceResourceFactory.createResource(davResourceLocator, davServletRequest, davServletResponse) instanceof NonExistentResource);
    }

    @Test
    public void testCreateResourceWithNonNumericYearToBlogYearFolder() throws DavException {
        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix, workspacePath, workspacePath + "/Global/" + space.getKey() + "/@news/._2008"
        );

        assertTrue(confluenceResourceFactory.createResource(davResourceLocator, davServletRequest, davServletResponse) instanceof NonExistentResource);
    }

    @Test
    public void testCreateResourceWithNonNumericYearToBlogMonthFolder() throws DavException {
        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix, workspacePath, workspacePath + "/Global/" + space.getKey() + "/@news/2008/._01"
        );

        assertTrue(confluenceResourceFactory.createResource(davResourceLocator, davServletRequest, davServletResponse) instanceof NonExistentResource);
    }

    @Test
    public void testCreateResourceWithNonNumericYearToBlogDayFolder() throws DavException {
        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix, workspacePath, workspacePath + "/Global/" + space.getKey() + "/@news/2008/01/._01"
        );

        assertTrue(confluenceResourceFactory.createResource(davResourceLocator, davServletRequest, davServletResponse) instanceof NonExistentResource);
    }

    @Test
    public void testCreateResourceWithPathToPageExportsReadme() throws DavException {
        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix, workspacePath, workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle() + "/@exports/README.txt"
        );

        assertTrue(confluenceResourceFactory.createResource(davResourceLocator, davServletRequest, davServletResponse) instanceof GeneratedResourceReadMeResource);
    }

    @Test
    public void testCreateResourceWithPathToPageVersionsReadme() throws DavException {
        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix, workspacePath, workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle() + "/@versions/README.txt"
        );

        assertTrue(confluenceResourceFactory.createResource(davResourceLocator, davServletRequest, davServletResponse) instanceof GeneratedResourceReadMeResource);
    }

    @Test
    public void testCreateResourceWithPathToSpaceAttachment() throws DavException {
        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix, workspacePath, workspacePath + "/Global/" + space.getKey() + "/.DS_Store"
        );

        SpaceDescription spaceDesc = new SpaceDescription();
        space.setDescription(spaceDesc);

        Attachment attachment = new Attachment();
        attachment.setFileName(".DS_Store");
        attachment.setContainer(spaceDesc);

        when(attachmentManager.getAttachment(spaceDesc, ".DS_Store")).thenReturn(attachment);

        assertTrue(confluenceResourceFactory.createResource(davResourceLocator, davServletRequest, davServletResponse) instanceof SpaceAttachmentResourceImpl);
    }

    @Test
    public void testCreateResourceWithPathThatEndsAtGlobalOrPersonalLevelButDoesNotPointToEitherResultsInNonExistentResourceReturned()
            throws DavException {
        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix, workspacePath, workspacePath + "/invalid"
        );

        assertTrue(confluenceResourceFactory.createResource(davResourceLocator, davServletRequest, davServletResponse) instanceof NonExistentResource);
    }

    @Test
    public void testCreateResourceWithPathPointingToValidPageButParentPathsNotMatchingPageAncestorTitlesReturnsNonExistentResource()
            throws DavException {
        final String parentPageTitle = "pageParent";

        page = new Page() {
            @Override
            public List<Page> getAncestors() {
                Page parentPage = new Page();
                parentPage.setSpace(space);
                parentPage.setTitle(parentPageTitle);

                return singletonList(parentPage);
            }
        };
        page.setTitle("test");
        page.setSpace(space);

        when(pageManager.getPage(space.getKey(), page.getTitle())).thenReturn(page);

        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix, workspacePath, workspacePath + "/Global/" + space.getKey() + "/invalidParentTitle/" + page.getTitle()
        );

        assertTrue(confluenceResourceFactory.createResource(davResourceLocator, davServletRequest, davServletResponse) instanceof NonExistentResource);
    }

    @Test
    public void testCreateResourceWithPathPointingToValidPageButDuplicateAncestorsReturnsPageResource()
            throws DavException {
        final String parentPageTitle = "pageParent";

        page = new Page() {
            @Override
            public List<Page> getAncestors() {
                Page parentPage = new Page();
                parentPage.setSpace(space);
                parentPage.setTitle(parentPageTitle);
                parentPage.setId(1);

                return Arrays.asList(parentPage, parentPage);
            }
        };
        page.setTitle("Test");
        page.setSpace(space);

        when(pageManager.getPage(space.getKey(), page.getTitle())).thenReturn(page);

        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix, workspacePath, workspacePath + "/Global/" + space.getKey() + "/" + parentPageTitle + "/" + page.getTitle()
        );

        assertTrue(confluenceResourceFactory.createResource(davResourceLocator, davServletRequest, davServletResponse) instanceof PageResourceImpl);
    }

    @Test
    public void testCreateResourceWithPathPointingToValidPageButParentPathsNotMatchingPageAncestorTitlesWithoutStrictPathCheckReturnsPageResource()
            throws DavException {
        final String parentPageTitle = "pageParent";

        page = new Page() {
            @Override
            public List<Page> getAncestors() {
                Page parentPage = new Page();
                parentPage.setSpace(space);
                parentPage.setTitle(parentPageTitle);

                return singletonList(parentPage);
            }
        };
        page.setTitle("Test");
        page.setSpace(space);

        when(webdavSettingsManager.isStrictPageResourcePathCheckingDisabled()).thenReturn(true);
        when(pageManager.getPage(space.getKey(), page.getTitle())).thenReturn(page);

        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix, workspacePath, workspacePath + "/Global/" + space.getKey() + "/invalidParentTitle/" + page.getTitle()
        );

        assertTrue(confluenceResourceFactory.createResource(davResourceLocator, davServletRequest, davServletResponse) instanceof PageResourceImpl);
    }

    @Test
    public void testCreateResourceWithPathPointingToPageAndIncorrectNumberOfAncestorsReturnsNonExistentResource() throws DavException {
        final String parentPageTitle = "pageParent";

        page = new Page() {
            @Override
            public List<Page> getAncestors() {
                Page parentPage = new Page();
                parentPage.setSpace(space);
                parentPage.setTitle(parentPageTitle);

                return singletonList(parentPage);
            }
        };
        page.setTitle("test");
        page.setSpace(space);


        when(pageManager.getPage(space.getKey(), page.getTitle())).thenReturn(page);

        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix, workspacePath, workspacePath + "/Global/" + space.getKey() + "/invalid/number/of/ancestors/" + page.getTitle()
        );

        assertTrue(confluenceResourceFactory.createResource(davResourceLocator, davServletRequest, davServletResponse) instanceof NonExistentResource);
    }
}
