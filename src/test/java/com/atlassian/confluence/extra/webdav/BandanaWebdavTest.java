package com.atlassian.confluence.extra.webdav;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.bandana.DefaultBandanaManager;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.setup.BootstrapManager;
import com.atlassian.confluence.setup.bandana.ConfluenceBandanaPersister;
import com.atlassian.confluence.setup.xstream.XStreamManager;
import com.atlassian.confluence.spaces.Space;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static java.util.Collections.emptyMap;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class BandanaWebdavTest {

    private static final String SPACE_KEY = "SPACE";
    private static final long CONTENT_ID = 1L;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private BootstrapManager bootstrapManager;
    @Mock
    private Space space;
    @Mock
    private ContentEntityObject content;

    private WebdavSettingsManager webdavSettingsManager;
    private ConfluenceDavSessionStore davSessionStore;

    @Before
    public void setup() {
        final ConfluenceBandanaPersister bandanaPersister = new ConfluenceBandanaPersister();
        bandanaPersister.setBootstrapManager(bootstrapManager);
        bandanaPersister.setxStreamManager(new XStreamManager(emptyMap()));

        final BandanaManager bandanaManager = new DefaultBandanaManager(bandanaPersister);

        webdavSettingsManager = new BandanaWebdavSettingsManager(bandanaManager);
        davSessionStore = new BandanaConfluenceDavSessionStore(bandanaManager);

        when(space.getKey()).thenReturn(SPACE_KEY);
        when(content.getId()).thenReturn(CONTENT_ID);
    }

    @Test
    public void testSaveSettings() {
        final WebdavSettings settings = new WebdavSettings();
        webdavSettingsManager.save(settings);
        assertThat(webdavSettingsManager.getWebdavSettings(), is(settings));
    }

    @Test
    public void testMapSession() {
        final String userName = "viqueen";
        final ConfluenceDavSession session = new ConfluenceDavSession(userName);
        session.getResourceStates().hideSpaceDescription(space);
        session.getResourceStates().hideContentExportsReadme(content);

        davSessionStore.mapSession(session, userName);

        final ConfluenceDavSession restoredSession = davSessionStore.getSession(userName);
        assertThat(restoredSession, notNullValue());
        assertThat(restoredSession.getUserName(), is(session.getUserName()));
        assertTrue(restoredSession.getResourceStates().isSpaceDescriptionHidden(space));
        assertTrue(restoredSession.getResourceStates().isContentExportsReadmeHidden(content));
    }
}
