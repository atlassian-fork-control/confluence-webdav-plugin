package com.atlassian.confluence.extra.webdav.util;

import com.atlassian.confluence.extra.webdav.ResourceStates;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.spaces.Space;
import junit.framework.TestCase;

public class ResourceStatesTestCase extends TestCase {
    private ResourceStates resourceStates;

    private Space space;

    private Page page;

    private Attachment attachment;

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        space = new Space("tst");
        page = new Page();
        page.setSpace(space);

        attachment = new Attachment();
        attachment.setFileName("foobar.txt");
        attachment.setContainer(page);

        resourceStates = new ResourceStates();
    }

    public void testResourceAttributeDefaults() {
        assertFalse(resourceStates.isSpaceDescriptionHidden(space));
        assertFalse(resourceStates.isContentHidden(page));
        assertFalse(resourceStates.isContentMarkupHidden(page));
        assertFalse(resourceStates.isContentUrlHidden(page));
        assertFalse(resourceStates.isContentVersionsHidden(page));
        assertFalse(resourceStates.isContentVersionTextHidden(page, "Version 1"));
        assertFalse(resourceStates.isContentVersionsReadmeHidden(page));
        assertFalse(resourceStates.isContentExportsHidden(page));
        assertFalse(resourceStates.isContentPdfExportHidden(page));
        assertFalse(resourceStates.isContentWordExportHidden(page));
        assertFalse(resourceStates.isContentExportsReadmeHidden(page));
        assertFalse(resourceStates.isAttachmentHidden(attachment));
    }

    public void testToggleSpaceDescriptionHidden() {
        resourceStates.hideSpaceDescription(space);
        assertTrue(resourceStates.isSpaceDescriptionHidden(space));

        resourceStates.unhideSpaceDescription(space);
        assertFalse(resourceStates.isSpaceDescriptionHidden(space));
    }

    public void testTogglePageHidden() {
        resourceStates.hideContent(page);
        assertTrue(resourceStates.isContentHidden(page));

        resourceStates.unhideContent(page);
        assertFalse(resourceStates.isContentHidden(page));
    }

    public void testTogglePageContentHidden() {
        resourceStates.hideContentMarkup(page);
        assertTrue(resourceStates.isContentMarkupHidden(page));

        resourceStates.unhideContentMarkup(page);
        assertFalse(resourceStates.isContentMarkupHidden(page));
    }

    public void testTogglePageUrlHidden() {
        resourceStates.hideContentUrl(page);
        assertTrue(resourceStates.isContentUrlHidden(page));

        resourceStates.unhideContentUrl(page);
        assertFalse(resourceStates.isContentUrlHidden(page));
    }

    public void testTogglePageVersionsDirectorylHidden() {
        resourceStates.hideContentVersions(page);
        assertTrue(resourceStates.isContentVersionsHidden(page));

        resourceStates.unhideContentVersions(page);
        assertFalse(resourceStates.isContentVersionsHidden(page));
    }

    public void testTogglePageVersionTextHidden() {
        String version1 = "Version 1";

        resourceStates.hideContentVersionText(page, version1);
        assertTrue(resourceStates.isContentVersionTextHidden(page, version1));

        resourceStates.unhideContentVersionText(page, version1);
        assertFalse(resourceStates.isContentVersionTextHidden(page, version1));
    }

    public void testTogglePageVersionsReadmeHidden() {
        resourceStates.hideContentVersionsReadme(page);
        assertTrue(resourceStates.isContentVersionsReadmeHidden(page));

        resourceStates.unhideContentVersionsReadme(page);
        assertFalse(resourceStates.isContentVersionsReadmeHidden(page));
    }

    public void testTogglePageExportsHidden() {
        resourceStates.hideContentExports(page);
        assertTrue(resourceStates.isContentExportsHidden(page));

        resourceStates.unhideContentExports(page);
        assertFalse(resourceStates.isContentExportsHidden(page));
    }

    public void testTogglePagePdfExportHidden() {
        resourceStates.hideContentPdfExport(page);
        assertTrue(resourceStates.isContentPdfExportHidden(page));

        resourceStates.unhideContentPdfExport(page);
        assertFalse(resourceStates.isContentPdfExportHidden(page));
    }

    public void testTogglePageMsWordExportHidden() {
        resourceStates.hideContentWordExport(page);
        assertTrue(resourceStates.isContentWordExportHidden(page));

        resourceStates.unhideContentWordExport(page);
        assertFalse(resourceStates.isContentWordExportHidden(page));
    }

    public void testTogglePageExportsReadmeHidden() {
        resourceStates.hideContentExportsReadme(page);
        assertTrue(resourceStates.isContentExportsReadmeHidden(page));

        resourceStates.unhideContentExportsReadme(page);
        assertFalse(resourceStates.isContentExportsReadmeHidden(page));
    }

    public void testTogglePageAttachmentsHidden() {
        resourceStates.hideAttachment(attachment);
        assertTrue(resourceStates.isAttachmentHidden(attachment));

        resourceStates.unhideAttachment(attachment);
        assertFalse(resourceStates.isAttachmentHidden(attachment));
    }
}
