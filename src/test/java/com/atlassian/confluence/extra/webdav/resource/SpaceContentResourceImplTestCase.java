package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.extra.webdav.ResourceStates;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.setup.settings.Settings;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceDescription;
import org.apache.commons.io.IOUtils;
import org.apache.jackrabbit.webdav.io.OutputContext;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SpaceContentResourceImplTestCase extends AbstractConfluenceResourceTestCase {

    private SpaceContentResourceImpl spaceContentResourceImpl;
    private Space space;

    @Before
    public void initialise() {
        space = new Space("ds");
        space.setName("demospace");

        when(spaceManager.getSpace(space.getKey())).thenReturn(space);
        when(permissionManager.hasPermission(user, Permission.VIEW, space)).thenReturn(true);

        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix, workspacePath,
                workspacePath + "/Global/" + space.getKey() + "/" + space.getName() + ".txt"
        );

        spaceContentResourceImpl = new SpaceContentResourceImpl(
                davResourceLocator, davResourceFactory, lockManager, davSession,
                settingsManager, spaceManager,
                space.getKey());
    }

    @Test
    public void testResourceDoesNotExistIfHidden() {
        ResourceStates resourceStates = davSession.getResourceStates();

        assertFalse(resourceStates.isSpaceDescriptionHidden(space));
        resourceStates.hideSpaceDescription(space);

        assertFalse(spaceContentResourceImpl.exists());
    }

    @Test
    public void testResourceExistsIfNotHiddenAndSpaceViewable() {
        assertTrue(spaceContentResourceImpl.exists());
    }

    @Test
    public void testUrlContentEncodedWithSpecifiedCharset() throws IOException {
        String expectedOutput = "Space Description";
        byte[] expectedOutputBytes = expectedOutput.getBytes("UTF-8");


        SpaceDescription spaceDescription = new SpaceDescription(space);
        spaceDescription.setBodyAsString(expectedOutput);
        space.setDescription(spaceDescription);
        space.setLastModificationDate(new Date());

        Settings globalSettings = new Settings();
        globalSettings.setDefaultEncoding("UTF-8");


        when(settingsManager.getGlobalSettings()).thenReturn(globalSettings);

        OutputContext outputContext = mock(OutputContext.class);


        InputStream in = null;
        ByteArrayOutputStream out = null;

        try {
            in = spaceContentResourceImpl.getContent();
            out = new ByteArrayOutputStream();


            when(outputContext.hasStream()).thenReturn(true);
            when(outputContext.getOutputStream()).thenReturn(out);

            spaceContentResourceImpl.spool(outputContext);

            assertEquals(
                    expectedOutput,
                    new String(out.toByteArray(), "UTF-8")
            );

            verify(outputContext).setContentLength(expectedOutputBytes.length);
            verify(outputContext).setContentType(spaceContentResourceImpl.getContentTypeBase());
            verify(outputContext).setModificationTime(space.getLastModificationDate().getTime());
        } finally {
            IOUtils.closeQuietly(out);
            IOUtils.closeQuietly(in);
        }
    }

    @Test
    public void testCreationDateEqualsSpaceCreationDate() {
        space.setCreationDate(new Date());
        assertEquals(space.getCreationDate().getTime(), spaceContentResourceImpl.getCreationtTime());
    }

    @Test
    public void testDisplayNameIsBasedOnSpaceName() {
        space.setName("Test Space");
        assertEquals(space.getName() + ".txt", spaceContentResourceImpl.getDisplayName());
    }
}
