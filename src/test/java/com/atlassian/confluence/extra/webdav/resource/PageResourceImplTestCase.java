package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.extra.webdav.job.ContentJob;
import com.atlassian.confluence.extra.webdav.job.ContentJobQueue;
import com.atlassian.confluence.extra.webdav.job.impl.AttachmentRemovalJob;
import com.atlassian.confluence.extra.webdav.util.ContainerManagerTestSupport;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.spaces.Space;
import com.google.common.collect.ImmutableList;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.DavResource;
import org.apache.jackrabbit.webdav.io.InputContext;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.argThat;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.notNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PageResourceImplTestCase extends AbstractConfluenceResourceTestCase {

    @Mock
    private DavResource davResource;
    @Mock
    private InputContext inputContext;

    private PageResourceImpl pageResource;
    private Attachment attachment;
    private Space space;
    private Page page;
    private Page pageParent;
    private Page pageChild;

    @Before
    public void initialise() {
        attachment = new Attachment();
        attachment.setFileName("testAttachment");
        attachment.setMediaType("application/octet-stream");
        attachment.setFileSize(0);

        space = new Space("ds");

        page = new Page();
        page.setId(1);
        page.setTitle("testPage");
        page.addAttachment(attachment);
        page.setSpace(space);

        attachment.setContainer(page);

        when(attachmentManager.getLatestVersionsOfAttachments(page)).thenReturn(ImmutableList.of(attachment));

        pageParent = new Page();
        pageParent.setId(2);
        pageParent.setTitle("testPageParent");
        pageParent.addChild(page);
        page.setParentPage(pageParent);

        pageChild = new Page();
        pageChild.setId(3);
        pageChild.setSpace(space);
        pageChild.setTitle("testPageChild");
        pageChild.setBodyAsString(StringUtils.EMPTY);
        pageChild.setParentPage(page);
        page.addChild(pageChild);

        when(spaceManager.getSpace(space.getKey())).thenReturn(space);
        when(pageManager.getPage(space.getKey(), pageParent.getTitle())).thenReturn(pageParent);
        when(pageManager.getPage(space.getKey(), page.getTitle())).thenReturn(page);

        davResourceLocator = davLocatorFactory.createResourceLocator(
                prefix,
                workspacePath,
                workspacePath + "/Global/" + space.getKey() + "/" + pageParent.getTitle() + "/" + page.getTitle()
        );

        pageResource = new PageResourceImpl(
                davResourceLocator, davResourceFactory, lockManager, davSession,
                permissionManager, spaceManager, pageManager, attachmentManager, contentJobQueue,
                space.getKey(), page.getTitle());

        new ContainerManagerTestSupport()
                .registerComponent("attachmentManager", attachmentManager)
                .registerComponent("confluenceUserDao",confluenceUserDao)
                .init();
    }

    @Test
    public void testAddPageAsMemberWithNoCreatePermission() {
        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + pageParent.getTitle() + "/" + page.getTitle() + "/newpage");

        try {
            pageResource.addMember(davResource, inputContext);
            fail("DavException expected here.");
        } catch (DavException e) {
            assertEquals(HttpServletResponse.SC_FORBIDDEN, e.getErrorCode());
            verify(permissionManager).hasCreatePermission(user, space, Page.class);
        }
    }

    @Test
    public void testAddPageAsMemberWithCreatePermission() throws DavException {
        final String newPagetitle = "newPagetitle";

        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + pageParent.getTitle() + "/" + page.getTitle() + "/" + newPagetitle);
        when(permissionManager.hasCreatePermission(user, space, Page.class)).thenReturn(true);

        pageResource.addMember(davResource, inputContext);

        verify(pageManager).saveContentEntity(
                argThat(
                        (ArgumentMatcher<Page>) page -> PageResourceImplTestCase.this.page == page.getParent()
                                && page.getSpace() == PageResourceImplTestCase.this.page.getSpace()
                                && StringUtils.equals(newPagetitle, page.getTitle())
                                && StringUtils.equals(user.getName(), page.getCreator().getName())
                ),
                any());
    }

    @Test
    public void testAddDsStoreResourceIgnored() throws DavException, IOException {
        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + pageParent.getTitle() + "/" + page.getTitle() + "/" + ".DS_Store");
        when(inputContext.hasStream()).thenReturn(true);
        when(permissionManager.hasCreatePermission(user, page, Attachment.class)).thenReturn(true);

        pageResource.addMember(davResource, inputContext);
        verify(attachmentManager, never()).saveAttachment(
                any(),
                any(),
                any()
        );
    }

    @Test
    public void testAddTextFileWithNameSameAsPageTitleUpdatesPageContent() throws DavException {
        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + pageParent.getTitle() + "/" + page.getTitle() + "/" + page.getTitle() + ".txt");
        when(inputContext.hasStream()).thenReturn(true);
        when(permissionManager.hasPermission(user, Permission.EDIT, page)).thenReturn(true);
        when(inputContext.getInputStream()).thenReturn(new ByteArrayInputStream("Foo".getBytes()));

        pageResource.addMember(davResource, inputContext);
        verify(pageManager).saveContentEntity(page, page, null);
    }

    @Test
    public void testAttachmentAddedAsMemberHasFileSizeEqualsToInputStreamLength() throws DavException, IOException {
        final String attachmentFileName = "attachmentFileName.txt";
        final byte[] attachmentData = "attachmentData".getBytes();

        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + pageParent.getTitle() + "/" + page.getTitle() + "/" + attachmentFileName);
        when(inputContext.hasStream()).thenReturn(true);
        when(permissionManager.hasCreatePermission(user, page, Attachment.class)).thenReturn(true);
        when(inputContext.getInputStream()).thenReturn(new ByteArrayInputStream(attachmentData));

        pageResource.addMember(davResource, inputContext);

        verify(attachmentManager).saveAttachment(
                argThat(
                        attachment -> attachment.getContainer() == page
                                && attachment.getFileSize() == attachmentData.length
                                && StringUtils.equals(attachmentFileName, attachment.getFileName())
                                && StringUtils.equals(user.getName(), attachment.getCreator().getName())
                ),
                any(),
                notNull());
    }

    @Test
    public void testAttachmentAddedAsMemberWithLocallyResolvedContentType() throws DavException, IOException {
        final String attachmentFileName = "attachmentFileName.txt";
        final byte[] attachmentData = "attachmentData".getBytes();

        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + pageParent.getTitle() + "/" + page.getTitle() + "/" + attachmentFileName);
        when(inputContext.hasStream()).thenReturn(true);
        when(permissionManager.hasCreatePermission(user, page, Attachment.class)).thenReturn(true);
        when(inputContext.getInputStream()).thenReturn(new ByteArrayInputStream(attachmentData));

        pageResource.addMember(davResource, inputContext);

        verify(attachmentManager).saveAttachment(
                argThat(
                        attachment -> attachment.getContainer() == page
                                && attachment.getFileSize() == attachmentData.length
                                && StringUtils.equals(attachmentFileName, attachment.getFileName())
                                && StringUtils.equals(user.getName(), attachment.getCreator().getName())
                                && StringUtils.equals("text/plain", attachment.getMediaType())
                ),
                any(),
                notNull());
    }

    @Test
    public void testRemoveChildPageWithoutPermission() {
        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + pageParent.getTitle() + "/" + page.getTitle() + "/" + pageChild.getTitle());

        try {
            pageResource.removeMember(davResource);
            fail("DavException expected here.");
        } catch (DavException e) {
            assertEquals(HttpServletResponse.SC_FORBIDDEN, e.getErrorCode());
            verify(permissionManager).hasPermission(user, Permission.REMOVE, pageChild);
        }
    }

    @Test
    public void testRemoveChildPageWithPermission() throws DavException {
        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + pageParent.getTitle() + "/" + page.getTitle() + "/" + pageChild.getTitle());
        when(permissionManager.hasPermission(user, Permission.REMOVE, pageChild)).thenReturn(true);

        pageResource.removeMember(davResource);
        verify(pageManager).trashPage(pageChild);
    }

    @Test
    public void testAttachmentRemovalPushedOntoRemoveQueue() throws DavException {
        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + pageParent.getTitle() + "/" + page.getTitle() + "/" + pageChild.getTitle() + "/" + attachment.getFileName());
        when(permissionManager.hasPermission(user, Permission.REMOVE, attachment)).thenReturn(true);

        pageResource.removeMember(davResource);
        verify(contentJobQueue).enque(new AttachmentRemovalJob(pageManager, attachmentManager, page.getId(), attachment.getFileName()));
    }

    @Test
    public void testAttachmentNotRemovedIfClientTriesToRemoveAndRecreateAttachmentAsUpdate() throws DavException, IOException {
        final AttachmentRemovalJob attachmentRemovalJobToBeCancelled =
                new AttachmentRemovalJob(pageManager, attachmentManager, page.getId(), attachment.getFileName());

        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + pageParent.getTitle() + "/" + page.getTitle() + "/" + pageChild.getTitle() + "/" + attachment.getFileName());
        when(permissionManager.hasPermission(user, Permission.REMOVE, attachment)).thenReturn(true);

        attachment.setId(1); /* Existing attachments have IDs */

        /* Some clients remove attachments first before they replace it */
        pageResource.removeMember(davResource);

        when(inputContext.hasStream()).thenReturn(true);
        when(permissionManager.hasCreatePermission(user, page, Attachment.class)).thenReturn(true);
        when(inputContext.getInputStream()).thenReturn(new ByteArrayInputStream(new byte[1]));

        /* Then those clients will recreate the attachment, with the updated content */
        pageResource.addMember(davResource, inputContext);

        verify(contentJobQueue).enque(attachmentRemovalJobToBeCancelled);
        verify(contentJobQueue).remove(attachmentRemovalJobToBeCancelled);

        verify(attachmentManager).saveAttachment(
                any(),
                any(),
                notNull()
        );
    }

    @Test
    public void testAttachmentRemovalTaskExecutedAfterTimeout() throws DavException {
        final StringBuffer jobExecuted = new StringBuffer();
        final AttachmentRemovalJob attachmentRemovalJobToBeExecuted = new AttachmentRemovalJob(
                pageManager, attachmentManager, page.getId(), attachment.getFileName());

        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + pageParent.getTitle() + "/" + page.getTitle() + "/" + pageChild.getTitle() + "/" + attachment.getFileName());
        when(permissionManager.hasPermission(user, Permission.REMOVE, attachment)).thenReturn(true);

        pageResource = new PageResourceImpl(
                davResourceLocator, davResourceFactory, lockManager, davSession,
                permissionManager, spaceManager, pageManager, attachmentManager,
                contentJobQueue = new ContentJobQueue(transactionTemplate) {
                    @Override
                    protected boolean isJobDueForExecution(ContentJob job) {
                        return true;
                    }

                    @Override
                    protected ContentJob executeTask(ContentJob job) {
                        jobExecuted.setLength(0);
                        jobExecuted.append(job.equals(attachmentRemovalJobToBeExecuted));

                        return job;
                    }
                },
                space.getKey(), page.getTitle()
        );
        pageResource.removeMember(davResource);

        contentJobQueue.executeTasks();
        assertTrue(BooleanUtils.toBoolean(jobExecuted.toString()));
    }

    @Test
    public void testAttachmentCreationWithZeroByteContentAllowed() throws DavException, IOException {
        final ByteArrayInputStream attachmentDataZeroByte = new ByteArrayInputStream(new byte[0]);

        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + pageParent.getTitle() + "/" + page.getTitle() + "/" + pageChild.getTitle() + "/" + attachment.getFileName());
        when(inputContext.hasStream()).thenReturn(true);
        when(permissionManager.hasCreatePermission(user, page, Attachment.class)).thenReturn(true);
        when(inputContext.getInputStream()).thenReturn(attachmentDataZeroByte);

        pageResource.addMember(davResource, inputContext);

        verify(attachmentManager).saveAttachment(
                argThat(
                        attachment -> StringUtils.equals(this.attachment.getFileName(), attachment.getFileName())
                ),
                any(),
                notNull()
        );
    }

    @Test
    public void testPersistentAttachmentUpdateWithZeroBytesIgnored() throws DavException, IOException {
        final ByteArrayInputStream attachmentDataZeroByte = new ByteArrayInputStream(new byte[0]);
        final ByteArrayInputStream attachmentDataLongerThanZeroByte = new ByteArrayInputStream(new byte[1]);

        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + pageParent.getTitle() + "/" + page.getTitle() + "/" + pageChild.getTitle() + "/" + attachment.getFileName());
        when(inputContext.hasStream()).thenReturn(true);
        when(permissionManager.hasCreatePermission(user, page, Attachment.class)).thenReturn(true);
        when(attachmentManager.getAttachment(page, attachment.getFileName())).thenReturn(attachment);
        when(inputContext.getInputStream()).thenReturn(attachmentDataZeroByte).thenReturn(attachmentDataLongerThanZeroByte);

        attachment.setId(1); /* Persistent attachments have IDs */

        /* Some clients (like Finder) loves to update attachments by updating existing attachments with an empty one,
         * and then updating that again with the content. That means, 2 revisions of an attachment will be created by those
         * clients. So, in this this, we just want one, and we assume that no users will ever want to create page
         * attachments with zero length.
         */
        pageResource.addMember(davResource, inputContext);
        pageResource.addMember(davResource, inputContext);

        verify(attachmentManager, times(1)).saveAttachment(
                argThat(
                        attachment -> StringUtils.equals(this.attachment.getFileName(), attachment.getFileName())
                ),
                any(),
                notNull()
        );
    }

    @Test
    public void testMovePageToBecomeSpaceDenied() {
        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey());

        try {
            pageResource.move(davResource);
            fail("DavException expected.");
        } catch (DavException de) {
            assertEquals(HttpServletResponse.SC_FORBIDDEN, de.getErrorCode());
        }
    }

    @Test
    public void testMovePageToBecomeBlogDenied() {
        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/@news/2008/01/01/test.txt");

        try {
            pageResource.move(davResource);
            fail("DavException expected.");
        } catch (DavException de) {
            assertEquals(HttpServletResponse.SC_FORBIDDEN, de.getErrorCode());
        }
    }

    @Test
    public void testMovePageToBecomeChildrenOfExportsDirectoryDenied() {
        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + pageParent.getTitle() + "/" + page.getTitle() + "/@exports/exportChild");

        try {
            pageResource.move(davResource);
            fail("DavException expected.");
        } catch (DavException de) {
            assertEquals(HttpServletResponse.SC_FORBIDDEN, de.getErrorCode());
        }
    }

    @Test
    public void testMovePageToBecomeChildrenOfVersionsDirectoryDenied() {
        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + pageParent.getTitle() + "/" + page.getTitle() + "/@version/versionChild");

        try {
            pageResource.move(davResource);
            fail("DavException expected.");
        } catch (DavException de) {
            assertEquals(HttpServletResponse.SC_FORBIDDEN, de.getErrorCode());
        }
    }

    @Test
    public void testMoveWithNoMovePermissionDenied() {
        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle());

        try {
            pageResource.move(davResource);
            fail("DavException expected here.");
        } catch (DavException e) {
            assertEquals(HttpServletResponse.SC_FORBIDDEN, e.getErrorCode());
            verify(permissionManager).hasPermission(user, Permission.EDIT, page);
        }
    }

    @Test
    public void testRenamePage() throws DavException {
        String renamedTitle = "renamedTitle";

        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + pageParent.getTitle() + "/" + renamedTitle);
        when(permissionManager.hasPermission(user, Permission.EDIT, page)).thenReturn(true);

        pageResource.move(davResource);
        verify(pageManager).renamePage(page, renamedTitle);
    }

    @Test
    public void testRenameToExistingPage() {
        Page existing = new Page();

        existing.setSpace(space);
        existing.setTitle("Existing page");

        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + existing.getTitle());
        when(permissionManager.hasPermission(user, Permission.EDIT, page)).thenReturn(true);
        when(pageManager.getPage(space.getKey(), existing.getTitle())).thenReturn(existing);

        try {
            pageResource.move(davResource);
            fail("DavException expected.");
        } catch (DavException de) {
            assertEquals(HttpServletResponse.SC_FORBIDDEN, de.getErrorCode());
        }
    }

    @Test
    public void testRenameWithInvalidTitle() {
        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + pageParent.getTitle() + "/Invalid @ page title");

        try {
            pageResource.move(davResource);
            fail("DavException expected.");
        } catch (DavException de) {
            assertEquals(HttpServletResponse.SC_FORBIDDEN, de.getErrorCode());
        }
    }

    @Test
    public void testMovePageHierarchyToAnotherParent() throws DavException {
        Page anotherParent = new Page();

        anotherParent.setSpace(space);
        anotherParent.setTitle("Parent");

        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + anotherParent.getTitle() + "/" + page.getTitle());
        when(permissionManager.hasPermission(user, Permission.EDIT, page)).thenReturn(true);
        when(pageManager.getPage(space.getKey(), anotherParent.getTitle())).thenReturn(anotherParent);


        pageResource.move(davResource);
        verify(pageManager).movePageAsChild(page, anotherParent);
    }

    @Test
    public void testMovePageHierarchyAsTopLevelPageInDifferentSpace() throws DavException {
        Space differentSpace = new Space("ds2");

        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + differentSpace.getKey() + "/" + page.getTitle());
        when(permissionManager.hasPermission(user, Permission.EDIT, page)).thenReturn(true);
        when(spaceManager.getSpace(differentSpace.getKey())).thenReturn(differentSpace);

        pageResource.move(davResource);
        verify(pageManager).movePageAsChild(page, null);
    }

    @Test
    public void testMovePageHierarchyAsNonTopLevelPageInDifferentSpace() throws DavException {
        Space differentSpace = new Space("ds2");
        Page parentPageInDifferentSpace = new Page();

        parentPageInDifferentSpace.setSpace(differentSpace);
        parentPageInDifferentSpace.setTitle("Parent");


        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + differentSpace.getKey() + "/" + parentPageInDifferentSpace.getTitle() + "/" + page.getTitle());
        when(permissionManager.hasPermission(user, Permission.EDIT, page)).thenReturn(true);
        when(pageManager.getPage(differentSpace.getKey(), parentPageInDifferentSpace.getTitle())).thenReturn(parentPageInDifferentSpace);
        when(spaceManager.getSpace(differentSpace.getKey())).thenReturn(differentSpace);

        pageResource.move(davResource);
        verify(pageManager).movePageAsChild(page, parentPageInDifferentSpace);
    }

    @Test
    public void testCopyWithNoPermission() {
        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + page.getTitle());

        try {
            pageResource.copy(davResource, false);
            fail("DavException expected here.");
        } catch (DavException e) {
            assertEquals(HttpServletResponse.SC_FORBIDDEN, e.getErrorCode());
            verify(permissionManager).hasCreatePermission(user, space, Page.class);
        }
    }

    @Test
    public void testCopyWithPermissionWithNoAttachmentAndNoChildPage() throws DavException {
        final String copiedPage = "Duplicate";

        page.setAttachments(new ArrayList<>());
        page.setChildren(new ArrayList<>());

        pageChild.setParentPage(null);
        attachment.setContainer(null);

        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + copiedPage);
        when(permissionManager.hasCreatePermission(user, space, Page.class)).thenReturn(true);

        pageResource.copy(davResource, false);

        verify(pageManager).saveContentEntity(
                argThat(
                        (ArgumentMatcher<Page>) page -> StringUtils.equals(copiedPage, page.getTitle())
                ),
                any());
    }

    @Test
    public void testCopyWithPermissionWithAttachmentAndChildPage() throws DavException, IOException {
        final String copiedPage = "Duplicate";
        final String attachmentData = "attachmentData";
        final byte[] attachmentDataBytes = attachmentData.getBytes(UTF_8);

        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + copiedPage);
        when(permissionManager.hasCreatePermission(user, space, Page.class)).thenReturn(true);
        when(attachmentManager.getAttachmentData(attachment)).thenReturn(new ByteArrayInputStream(attachmentDataBytes));
        when(pageManager.getPage(space.getKey(), pageChild.getTitle())).thenReturn(pageChild);

        pageResource.copy(davResource, false);

        verify(attachmentManager).saveAttachment(
                argThat(
                        attachment -> null != attachment.getContainer() && attachment.getContainer() != page
                ),
                any(),
                any()
        );


        verify(pageManager).saveContentEntity(
                argThat(
                        (ArgumentMatcher<Page>) page -> StringUtils.equals("Copy of " + pageChild.getTitle(), page.getTitle())
                ),
                any());

        verify(pageManager).saveContentEntity(
                argThat(
                        (ArgumentMatcher<Page>) page -> StringUtils.equals(copiedPage, page.getTitle())
                ),
                any());
    }

    @Test
    public void testPageContentHiddenOnRemoval() throws DavException {
        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + pageParent.getTitle() + "/" + page.getTitle() + "/" + page.getTitle() + ".txt");
        pageResource.removeMember(davResource);

        assertTrue(davSession.getResourceStates().isContentMarkupHidden(page));
    }

    @Test
    public void testPageUrlHiddenOnRemoval() throws DavException {
        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + pageParent.getTitle() + "/" + page.getTitle() + "/" + page.getTitle() + ".url");
        pageResource.removeMember(davResource);

        assertTrue(davSession.getResourceStates().isContentUrlHidden(page));
    }

    @Test
    public void testPageExportsHiddenOnRemove() throws DavException {
        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + pageParent.getTitle() + "/" + page.getTitle() + "/@exports");
        pageResource.removeMember(davResource);

        assertTrue(davSession.getResourceStates().isContentExportsHidden(page));
    }

    @Test
    public void testPageVersionsHiddenOnRemove() throws DavException {
        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + pageParent.getTitle() + "/" + page.getTitle() + "/@versions");
        pageResource.removeMember(davResource);

        assertTrue(davSession.getResourceStates().isContentVersionsHidden(page));
    }

    @Test
    public void testAddPageExportsDirectoryAsMemberUnhidesIt() throws DavException {
        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + pageParent.getTitle() + "/" + page.getTitle() + "/@exports");
        when(permissionManager.hasCreatePermission(user, space, Page.class)).thenReturn(true);

        davSession.getResourceStates().hideContentExports(page);
        assertTrue(davSession.getResourceStates().isContentExportsHidden(page));

        pageResource.addMember(davResource, inputContext);

        assertFalse(davSession.getResourceStates().isContentExportsHidden(page));
    }

    @Test
    public void testAddPageVersionsDirectoryAsMemberUnhidesIt() throws DavException {
        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + pageParent.getTitle() + "/" + page.getTitle() + "/@versions");
        when(permissionManager.hasCreatePermission(user, space, Page.class)).thenReturn(true);

        davSession.getResourceStates().hideContentVersions(page);
        assertTrue(davSession.getResourceStates().isContentVersionsHidden(page));

        pageResource.addMember(davResource, inputContext);

        assertFalse(davSession.getResourceStates().isContentVersionsHidden(page));
    }

    @Test
    public void testAddPageUrlAsMemberUnhidesIt() throws DavException {
        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + pageParent.getTitle() + "/" + page.getTitle() + "/" + page.getTitle() + ".url");
        when(inputContext.hasStream()).thenReturn(true);
        when(permissionManager.hasCreatePermission(user, page, Attachment.class)).thenReturn(true);

        davSession.getResourceStates().hideContentUrl(page);
        assertTrue(davSession.getResourceStates().isContentUrlHidden(page));

        pageResource.addMember(davResource, inputContext);

        assertFalse(davSession.getResourceStates().isContentUrlHidden(page));
    }

    @Test
    public void testCreatePageWithTextEditTempFolderDenied() {

        final String textEditTempPageTitle = "(A Document Being Saved By TextEdit)";

        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + pageParent.getTitle() + "/" + textEditTempPageTitle);
        when(permissionManager.hasCreatePermission(user, space, Page.class)).thenReturn(true);

        davSession.setUserAgent("WebDAVFS/1.0");

        try {
            pageResource.addMember(davResource, inputContext);
            fail();
        } catch (DavException e) {
            assertEquals(HttpServletResponse.SC_FORBIDDEN, e.getErrorCode());
        }
    }

    @Test
    public void testCreateFinderTempVersionOfPageContentWithInvalidAttachmentFileNameCharactersInTitleAsAttachment() throws DavException, IOException {
        final String updateContent = "updateBytes";
        final byte[] updateContentBytes = updateContent.getBytes(UTF_8);

        page.setTitle("&+?=");

        when(pageManager.getPage(space.getKey(), page.getTitle())).thenReturn(page);
        when(davResource.getResourcePath()).thenReturn(workspacePath + "/Global/" + space.getKey() + "/" + pageParent.getTitle() + "/" + page.getTitle() + "/._" + page.getTitle() + ".txt");
        when(inputContext.hasStream()).thenReturn(true);
        when(permissionManager.hasCreatePermission(user, page, Attachment.class)).thenReturn(true);
        when(attachmentManager.getAttachment(page, "._" + page.getTitle() + ".txt")).thenReturn(null).thenReturn(attachment);
        when(inputContext.getInputStream()).thenReturn(new ByteArrayInputStream(updateContentBytes));

        pageResource = new PageResourceImpl(
                davResourceLocator, davResourceFactory, lockManager, davSession,
                permissionManager, spaceManager, pageManager, attachmentManager, contentJobQueue,
                space.getKey(), page.getTitle());

        pageResource.addMember(davResource, inputContext);

        verify(attachmentManager).saveAttachment(
                argThat(
                        attachment -> StringUtils.equals(
                                "._" + page.getTitle() + ".txt",
                                attachment.getFileName())
                ),
                any(),
                notNull()
        );
    }

    @Test
    public void testChildResourcesContainsUrlAttachmentsExportsVersionsAndContent() {
        when(attachmentManager.getLatestVersionsOfAttachments(page)).thenReturn(singletonList(attachment));
        when(permissionManager.getPermittedEntities(user, Permission.VIEW, singletonList(pageChild))).thenReturn(
                singletonList(pageChild)
        );
        when(attachmentManager.getAttachment(page, attachment.getFileName())).thenReturn(attachment);
        when(pageManager.getPage(space.getKey(), pageChild.getTitle())).thenReturn(pageChild);
        when(permissionManager.hasPermission(eq(user), eq(Permission.VIEW), any())).thenReturn(true);

        List<DavResource> childResources = new ArrayList<>(pageResource.getMemberResources());

        assertEquals(6, childResources.size());

        PageContentResourceImpl pageContentResource = (PageContentResourceImpl) childResources.get(0);
        assertEquals(page, pageContentResource.getPage());

        PageUrlResourceImpl pageUrlResource = (PageUrlResourceImpl) childResources.get(1);
        assertEquals(page, pageUrlResource.getPage());

        PageAttachmentResourceImpl pageAttachmentResource = (PageAttachmentResourceImpl) childResources.get(2);
        assertEquals(page, pageAttachmentResource.getContentEntityObject());

        PageExportsResourceImpl pageExportsResource = (PageExportsResourceImpl) childResources.get(3);
        assertEquals(page, pageExportsResource.getContentEntityObject());

        PageVersionsResourceImpl pageVersionsResource = (PageVersionsResourceImpl) childResources.get(4);
        assertEquals(page, pageVersionsResource.getContentEntityObject());

        PageResourceImpl pageResource = (PageResourceImpl) childResources.get(5);
        assertEquals(pageChild, pageResource.getPage());
    }

    @Test
    public void testCreationTimeEqualsPageCreationTime() {
        page.setCreationDate(new Date());

        assertEquals(
                page.getCreationDate().getTime(),
                pageResource.getCreationtTime()
        );
    }

    @Test
    public void testDisplayNameEqualsPageTitle() {
        assertEquals(page.getTitle(), pageResource.getDisplayName());
    }
}
