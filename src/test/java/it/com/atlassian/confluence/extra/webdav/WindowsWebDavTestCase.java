package it.com.atlassian.confluence.extra.webdav;

import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.client.methods.OptionsMethod;

import java.io.IOException;
import java.util.Arrays;

public class WindowsWebDavTestCase extends AbstractWebDavTestCase {
    /**
     * Tests to make sure that the confluence webdav servlet will be called if the
     * request is for the webdav root and there is no ending '/'. This is how XP web
     * folders does discovery.
     */
    public void testNoEndingPathSeparatorWorks() {
        String webDavRootPath = "/plugins/servlet/confluence";
        String targetUrl = new StringBuffer("http://")
                .append(getWebdavServerHostName())
                .append(':').append(getWebdavServerPort())
                .append('/').append(getWebdavServerContextPath())
                .append(webDavRootPath).toString();

        OptionsMethod optionsMethod = new OptionsMethod(targetUrl);

        try {
            httpClient.executeMethod(optionsMethod);
            String[] allowed = optionsMethod.getAllowedMethods();

            Arrays.sort(allowed);
            Arrays.binarySearch(allowed, "LOCK");
            Arrays.binarySearch(allowed, "UNLOCK");
            Arrays.binarySearch(allowed, "PROPFIND");
            Arrays.binarySearch(allowed, "PUT");
        } catch (HttpException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Tests to make sure that the Confluence index will be updated after
     * making a page content update from WebDAV
     */
    public void testConfluenceIndexAfterUpdatePageContent() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Index/Index.txt";
        PutMethod putMethod = new PutMethod(targetUrl);

        String pageContent = "There is something now";

        RequestEntity requestEntity = new StringRequestEntity(pageContent);
        putMethod.setRequestEntity(requestEntity);

        try {
            int statusCode = httpClient.executeMethod(putMethod);

            assertEquals(statusCode, HttpStatus.SC_NO_CONTENT);
        } finally {
            putMethod.releaseConnection();
        }

        pause((1000 * 60) + (1000 * 30));

        gotoPage("/display/ds/Index");
        assertLinkPresentWithText("Index");
        assertTextPresent(pageContent);
    }
}
