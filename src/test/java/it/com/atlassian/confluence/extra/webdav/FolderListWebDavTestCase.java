package it.com.atlassian.confluence.extra.webdav;

import com.atlassian.confluence.plugin.functest.helper.BlogPostHelper;
import com.atlassian.confluence.plugin.functest.helper.PageHelper;
import com.atlassian.confluence.plugin.functest.helper.SpaceHelper;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.jackrabbit.webdav.DavConstants;
import org.apache.jackrabbit.webdav.DavException;
import org.apache.jackrabbit.webdav.MultiStatus;
import org.apache.jackrabbit.webdav.MultiStatusResponse;
import org.apache.jackrabbit.webdav.client.methods.CopyMethod;
import org.apache.jackrabbit.webdav.client.methods.MkColMethod;
import org.apache.jackrabbit.webdav.client.methods.MoveMethod;
import org.apache.jackrabbit.webdav.client.methods.PropFindMethod;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

/**
 * Automated Functional Test
 * <p>
 * This class will be testing operations based on folder manipulation. Such as fetch/create/move/delete/copy page.
 *
 * @author weiching.cher
 */
public class FolderListWebDavTestCase extends AbstractWebDavTestCase {
    // ###### < READING  > ################################################################
    // ------ <  Global  > ----------------------------------------------------------------

    /**
     * Test getting the top level folders, &quot;Global&quot; and &quot;Personal&quot;.
     *
     * @throws IOException
     * @throws DavException
     */
    public void testGetTopLevelFolders() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl();
        PropFindMethod propFindMethod = new PropFindMethod(targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(3, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Global/",
                            getWebdavServletUrl() + "/Personal/",
                            getWebdavServletUrl() + "/"
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }
    }

    /**
     * Test getting the global space keys as folders under &quot;/Global&quot;.
     *
     * @throws IOException
     * @throws DavException
     */
    public void testGetGlobalSpacesAsFolders() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global";
        PropFindMethod propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(2, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Global/",
                            getWebdavServletUrl() + "/Global/ds/",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }
    }

    /**
     * Test PROPFIND command by browsing to news (blog) directory
     *
     * @throws IOException
     * @throws DavException
     */
    public void testGetGlobalBlogAsFolders() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/@news/2004/11/21";
        PropFindMethod propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(2, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Global/ds/%40news/2004/11/21/",
                            getWebdavServletUrl() + "/Global/ds/%40news/2004/11/21/Octagon%20blog%20post.txt",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            actualMultiStatusResponseHrefs.get(0);


            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }
    }

    // Test disabled due to flakeyness in Bamboo build
    /**
     * WBDV-109
     * Issue tracked - http://developer.atlassian.com/jira/browse/WBDV-109
     *
     * @throws IOException
     * @throws DavException
     */
//    public void testGetGlobalVersionsAsFolders() throws IOException, DavException
//    {
//        String targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/@versions";
//        PropFindMethod propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        try
//        {
//            MultiStatus multiStatus;
//            MultiStatusResponse[] multiStatusResponses;
//            List expectedMultiStatusResponseHrefs;
//            List actualMultiStatusResponseHrefs;
//
//            httpClient.executeMethod(propFindMethod);
//
//            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
//            multiStatusResponses = multiStatus.getResponses();
//
//            assertNotNull(multiStatusResponses);
//            assertEquals(4, multiStatusResponses.length);
//
//            expectedMultiStatusResponseHrefs = Arrays.asList(
//                    new String[]{
//                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40versions/",
//                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40versions/Version%2060.txt",
//                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40versions/Version%2061.txt",
//                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40versions/README.txt"
//                    }
//            );
//            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);
//
//            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
//            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
//        }
//        finally
//        {
//            /* Always always call releaseConnection() in finally. */
//            propFindMethod.releaseConnection();
//        }
//    }

    /**
     * Test PROPFIND command by browsing Global Space directory
     *
     * @throws IOException
     * @throws DavException
     */
    public void testGetGlobalPagesAsFolders() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds";
        PropFindMethod propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(5, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Global/ds/",
                            getWebdavServletUrl() + "/Global/ds/%40news/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                            getWebdavServletUrl() + "/Global/ds/Index/",
                            getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }
    }

    /**
     * Test PROPFIND command by browsing child page
     *
     * @throws IOException
     * @throws DavException
     */
    public void testGetGlobalChildPagesAsFolders() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview";
        PropFindMethod propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(21, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40exports/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40versions/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Email%20archiving/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Example%20Index/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Formatting%20Content/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Jira%20integration/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/News/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/RSS%20Capabilities/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Search%20Confluence%20content/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Source%20code/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Tasklist/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Thumbnail%20gallery/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Using%20Spaces/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Confluence%20Overview.txt",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Confluence%20Overview.url",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/excelspreadsheet.xls",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/harbour.jpg",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/pdfdoc.pdf",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/worddocument.doc",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }
    }

    /**
     * Test PROPFIND command by browsing Global Space to check for newly added space
     *
     * @throws IOException
     * @throws DavException
     */
    public void testGetGlobalNewSpaceAsFolders() throws IOException, DavException {
        SpaceHelper spaceHelper = getSpaceHelper();

        spaceHelper.setKey("spaceofspace");
        spaceHelper.setName("Space of space");

        assertTrue(spaceHelper.create());

        //gotoPage("/spaces/browsespace.action?key=" + spaceHelper.getKey());

        String targetUrl = getWebdavServletUrl() + "/Global";
        PropFindMethod propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(3, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Global/",
                            getWebdavServletUrl() + "/Global/ds/",
                            getWebdavServletUrl() + "/Global/spaceofspace/",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));

            assertTrue(spaceHelper.delete());
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }
    }

    /**
     * Test PROPFIND command by browsing newly created page
     *
     * @throws IOException
     * @throws DavException
     */
    public void testGetGlobalNewPageAsFolders() throws IOException, DavException {
        PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey("ds");
        pageHelper.setTitle("Page of page");
        pageHelper.setContent("page content");

        assertTrue(pageHelper.create());

        String targetUrl = getWebdavServletUrl() + "/Global/ds";
        PropFindMethod propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(6, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Global/ds/",
                            getWebdavServletUrl() + "/Global/ds/%40news/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                            getWebdavServletUrl() + "/Global/ds/Index/",
                            getWebdavServletUrl() + "/Global/ds/Page%20of%20page/",
                            getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));

            assertTrue(pageHelper.delete());
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }
    }

    /**
     * Test PROPFIND command by browsing newly created news (blog)
     *
     * @throws IOException
     * @throws DavException
     */
    public void testGetGlobalNewBlogAsFolders() throws IOException, DavException {
        BlogPostHelper blogPostHelper = getBlogPostHelper();
        Calendar cal = Calendar.getInstance();
        cal.set(2008, 4, 15);

        blogPostHelper.setSpaceKey("ds");
        blogPostHelper.setCreationDate(cal.getTime());
        blogPostHelper.setTitle("Blog of blog");
        blogPostHelper.setContent("blog content");

        assertTrue(blogPostHelper.create());

        String targetUrl = getWebdavServletUrl() + "/Global/ds/@news/2008/05/15";
        PropFindMethod propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(2, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Global/ds/%40news/2008/05/15/",
                            getWebdavServletUrl() + "/Global/ds/%40news/2008/05/15/Blog%20of%20blog.txt",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));

            assertTrue(blogPostHelper.delete());
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }
    }

    // Test disabled due to flakeyness in Bamboo build
    /**
     * WBDV-103, WBDV-109
     * Issue tracked - http://developer.atlassian.com/jira/browse/WBDV-103
     * Issue tracked - http://developer.atlassian.com/jira/browse/WBDV-109
     *
     * @throws IOException
     * @throws DavException
     */
//    public void testGetGlobalNewVersionNumberAsFolders() throws IOException, DavException
//    {
//        PageHelper pageHelper = getPageHelper();
//
//        pageHelper.setSpaceKey("ds");
//        pageHelper.setTitle("Confluence Overview");
//        pageHelper.setVersion(61);
//        pageHelper.setId(pageHelper.findBySpaceKeyAndTitle());
//        String content = pageHelper.getContent();
//
//        content += "added";
//        pageHelper.setContent(content);
//
//        assertTrue(pageHelper.update());
//
//        String targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/@versions";
//        PropFindMethod propFindMethod = new PropFindMethod(
//                targetUrl,
//                DavConstants.PROPFIND_ALL_PROP,
//                DavConstants.DEPTH_1);
//
//        try
//        {
//            MultiStatus multiStatus;
//            MultiStatusResponse[] multiStatusResponses;
//            List expectedMultiStatusResponseHrefs;
//            List actualMultiStatusResponseHrefs;
//
//            httpClient.executeMethod(propFindMethod);
//
//            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
//            multiStatusResponses = multiStatus.getResponses();
//
//            assertNotNull(multiStatusResponses);
//            assertEquals(5, multiStatusResponses.length);
//
//            expectedMultiStatusResponseHrefs = Arrays.asList(
//                    new String[]{
//                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40versions/",
//                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40versions/Version%2060.txt",
//                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40versions/Version%2061.txt",
//                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40versions/Version%2062.txt",
//                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40versions/README.txt"
//                    }
//            );
//            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);
//
//            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
//            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
//        }
//        finally
//        {
//            /* Always always call releaseConnection() in finally. */
//            propFindMethod.releaseConnection();
//        }
//    }

    /**
     * WBDV-87
     * Issue tracked - http://developer.atlassian.com/jira/browse/WBDV-87
     *
     * @throws IOException
     * @throws DavException
     */
    public void testGetGlobalExportAsFolders() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40exports";
        PropFindMethod propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(4, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40exports/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40exports/Confluence%20Overview.pdf",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40exports/Confluence%20Overview.doc",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40exports/README.txt"
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }
    }

    /**
     * Test MKCOL command by creating Scandinavian Characters as page title
     * Test PROPFIND command by browsing page with Scandinavian Characters
     *
     * @throws IOException
     * @throws DavException
     */
    public void testGetGlobalScandinavianCharactersAsFolders() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/sv%c3%adnakj%c3%b6t";
        MkColMethod mkColMethod = new MkColMethod(targetUrl);

        try {
            int statusCode = httpClient.executeMethod(mkColMethod);
            assertEquals(statusCode, HttpStatus.SC_CREATED);
        } finally {
            /* Always always call releaseConnection() in finally. */
            mkColMethod.releaseConnection();
        }

        targetUrl = getWebdavServletUrl() + "/Global/ds";
        PropFindMethod propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(6, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Global/ds/",
                            getWebdavServletUrl() + "/Global/ds/%40news/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                            getWebdavServletUrl() + "/Global/ds/Index/",
                            getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt",
                            getWebdavServletUrl() + "/Global/ds/sv%c3%adnakj%c3%b6t/",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));

            //assertTrue(pageHelper.delete());
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }
    }

    // ------ < Personal > ----------------------------------------------------------------

    /**
     * Test getting the personal space keys as folders under &quot;/Personal&quot;.
     *
     * @throws IOException
     * @throws DavException
     */
    public void testGetPersonalSpacesAsFolders() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Personal";
        PropFindMethod propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(2, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Personal/",
                            getWebdavServletUrl() + "/Personal/~admin/",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }
    }

    /**
     * Test PROPFIND command by browsing page in Personal Space
     *
     * @throws IOException
     * @throws DavException
     */
    public void testGetPersonalPagesAsFolders() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Personal/~admin";
        PropFindMethod propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(4, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Personal/~admin/",
                            getWebdavServletUrl() + "/Personal/~admin/%40news/",
                            getWebdavServletUrl() + "/Personal/~admin/Home/",
                            getWebdavServletUrl() + "/Personal/~admin/admin.txt",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }
    }

    /**
     * Test PROPFIND command by browsing page versions folder in Personal Space
     *
     * @throws IOException
     * @throws DavException
     */
    public void testGetPersonalVersionsAsFolders() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Personal/~admin/Home/%40versions";
        PropFindMethod propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(5, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Personal/~admin/Home/%40versions/",
                            getWebdavServletUrl() + "/Personal/~admin/Home/%40versions/Version%201.txt",
                            getWebdavServletUrl() + "/Personal/~admin/Home/%40versions/Version%202.txt",
                            getWebdavServletUrl() + "/Personal/~admin/Home/%40versions/Version%203.txt",
                            getWebdavServletUrl() + "/Personal/~admin/Home/%40versions/README.txt",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }
    }

    /**
     * Test PROPFIND command by browsing Personal Space's news (blog)
     *
     * @throws IOException
     * @throws DavException
     */
    public void testGetPersonalBlogAsFolders() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Personal/~admin/%40news";
        PropFindMethod propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(2, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Personal/~admin/%40news/",
                            getWebdavServletUrl() + "/Personal/~admin/%40news/2008/",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }
    }

    /**
     * Test PROFIND command by browsing page exports folder in Personal Space
     *
     * @throws IOException
     * @throws DavException
     */
    public void testGetPersonalExportAsFolders() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Personal/~admin/Home/%40exports";
        PropFindMethod propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(4, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Personal/~admin/Home/%40exports/",
                            getWebdavServletUrl() + "/Personal/~admin/Home/%40exports/Home.pdf",
                            getWebdavServletUrl() + "/Personal/~admin/Home/%40exports/Home.doc",
                            getWebdavServletUrl() + "/Personal/~admin/Home/%40exports/README.txt",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }
    }

    // ###### < WRITING  > ################################################################
    // ------ <  Global  > ----------------------------------------------------------------

    /**
     * Test MKCOL command by creating new page in Global Space
     *
     * @throws IOException
     * @throws DavException
     */
    public void testMkColGlobalCreateNewPageAsFolders() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/newpage";
        MkColMethod mkColMethod = new MkColMethod(targetUrl);

        try {
            int statusCode = httpClient.executeMethod(mkColMethod);
            assertEquals(statusCode, HttpStatus.SC_CREATED);
        } finally {
    		/* Always always call releaseConnection() in finally. */
            mkColMethod.releaseConnection();
        }

        targetUrl = getWebdavServletUrl() + "/Global/ds";
        PropFindMethod propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(6, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Global/ds/",
                            getWebdavServletUrl() + "/Global/ds/%40news/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                            getWebdavServletUrl() + "/Global/ds/Index/",
                            getWebdavServletUrl() + "/Global/ds/newpage/",
                            getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }
    }

    /**
     * Test DELETE command by removing page in Global Space
     *
     * @throws IOException
     * @throws DavException
     */
    public void testDelGlobalRemovePageAsFolders() throws IOException, DavException {
        PageHelper pageHelper = getPageHelper();
        pageHelper.setSpaceKey("ds");
        pageHelper.setTitle("newpage");

        assertTrue(pageHelper.create());

        String targetUrl = getWebdavServletUrl() + "/Global/ds";
        PropFindMethod propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(6, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Global/ds/",
                            getWebdavServletUrl() + "/Global/ds/%40news/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                            getWebdavServletUrl() + "/Global/ds/Index/",
                            getWebdavServletUrl() + "/Global/ds/newpage/",
                            getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }

        targetUrl = getWebdavServletUrl() + "/Global/ds/newpage";
        DeleteMethod deleteMethod = new DeleteMethod(targetUrl);

        try {
            int statusCode = httpClient.executeMethod(deleteMethod);
            assertEquals(statusCode, HttpStatus.SC_NO_CONTENT);
        } finally {
            deleteMethod.releaseConnection();
        }

        targetUrl = getWebdavServletUrl() + "/Global/ds";
        propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(5, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Global/ds/",
                            getWebdavServletUrl() + "/Global/ds/%40news/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                            getWebdavServletUrl() + "/Global/ds/Index/",
                            getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }
    }

    /**
     * WBDV-41
     * Issue tracked - http://developer.atlassian.com/jira/browse/WBDV-41
     *
     * @throws IOException
     * @throws DavException
     */
    public void testMoveGlobalRenamePageAsFolders() throws IOException, DavException {
        PageHelper pageHelper = getPageHelper();
        pageHelper.setSpaceKey("ds");
        pageHelper.setTitle("newpage");

        assertTrue(pageHelper.create());

        String targetUrl = getWebdavServletUrl() + "/Global/ds";
        PropFindMethod propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(6, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Global/ds/",
                            getWebdavServletUrl() + "/Global/ds/%40news/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                            getWebdavServletUrl() + "/Global/ds/Index/",
                            getWebdavServletUrl() + "/Global/ds/newpage/",
                            getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }

        targetUrl = getWebdavServletUrl() + "/Global/ds/newpage";
        String destinationUrl = getWebdavServletUrl() + "/Global/ds/pagenew";
        MoveMethod moveMethod = new MoveMethod(targetUrl, destinationUrl, false);

        try {
            int statusCode = httpClient.executeMethod(moveMethod);
            assertEquals(statusCode, HttpStatus.SC_CREATED);
        } finally {
            moveMethod.releaseConnection();
        }

        targetUrl = getWebdavServletUrl() + "/Global/ds";
        propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(6, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Global/ds/",
                            getWebdavServletUrl() + "/Global/ds/%40news/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                            getWebdavServletUrl() + "/Global/ds/Index/",
                            getWebdavServletUrl() + "/Global/ds/pagenew/",
                            getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }
    }

    /**
     * Test MOVE command by renaming page to another page that has the same name in Global Space
     *
     * @throws IOException
     * @throws DavException
     */
    public void testMoveGlobalRenamePageToExistingPageAsFolders() throws IOException, DavException {
        PageHelper pageHelper = getPageHelper();
        pageHelper.setSpaceKey("ds");
        pageHelper.setTitle("Index2");

        assertTrue(pageHelper.create());

        String targetUrl = getWebdavServletUrl() + "/Global/ds";
        PropFindMethod propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(6, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Global/ds/",
                            getWebdavServletUrl() + "/Global/ds/%40news/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                            getWebdavServletUrl() + "/Global/ds/Index/",
                            getWebdavServletUrl() + "/Global/ds/Index2/",
                            getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }

        targetUrl = getWebdavServletUrl() + "/Global/ds/Index2";
        String destinationUrl = getWebdavServletUrl() + "/Global/ds/Index";
        MoveMethod moveMethod = new MoveMethod(targetUrl, destinationUrl, false);

        try {
            int statusCode = httpClient.executeMethod(moveMethod);
            assertEquals(statusCode, HttpStatus.SC_PRECONDITION_FAILED);
        } finally {
            moveMethod.releaseConnection();
        }

        targetUrl = getWebdavServletUrl() + "/Global/ds";
        propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(6, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Global/ds/",
                            getWebdavServletUrl() + "/Global/ds/%40news/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                            getWebdavServletUrl() + "/Global/ds/Index/",
                            getWebdavServletUrl() + "/Global/ds/Index2/",
                            getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }
    }

    /**
     * Test COPY command by copying page to same location
     *
     * @throws IOException
     * @throws DavException
     */
    public void testCopyGlobalPageAsFolder() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking";
        String destinationUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)";
        CopyMethod copyMethod = new CopyMethod(targetUrl, destinationUrl, false);

        try {
            int statusCode = httpClient.executeMethod(copyMethod);

            assertEquals(statusCode, HttpStatus.SC_CREATED);
        } finally {
            copyMethod.releaseConnection();
        }

        targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview";
        PropFindMethod propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(22, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40exports/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40versions/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Email%20archiving/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Example%20Index/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Formatting%20Content/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Jira%20integration/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/News/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/RSS%20Capabilities/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Search%20Confluence%20content/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Source%20code/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Tasklist/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Thumbnail%20gallery/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Using%20Spaces/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Confluence%20Overview.txt",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Confluence%20Overview.url",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/excelspreadsheet.xls",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/harbour.jpg",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/pdfdoc.pdf",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/worddocument.doc",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }

        targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)";
        propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(6, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)/%40exports/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)/%40versions/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)/Copy%20of%20Breadcrumb%20demonstration/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)/Creating%20pages%20and%20linking%20(2).txt",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)/Creating%20pages%20and%20linking%20(2).url",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }

        targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)/Copy%20of%20Breadcrumb%20demonstration";
        propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(5, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)/Copy%20of%20Breadcrumb%20demonstration/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)/Copy%20of%20Breadcrumb%20demonstration/%40exports/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)/Copy%20of%20Breadcrumb%20demonstration/%40versions/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)/Copy%20of%20Breadcrumb%20demonstration/Copy%20of%20Breadcrumb%20demonstration.txt",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)/Copy%20of%20Breadcrumb%20demonstration/Copy%20of%20Breadcrumb%20demonstration.url",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }
    }


    /**
     * Test COPY command by copying page to same location multiple times
     *
     * @throws IOException
     * @throws DavException
     */
    public void testCopyGlobalPageMultipleTimesAsFolder() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking";
        String destinationUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)";
        CopyMethod copyMethod = new CopyMethod(targetUrl, destinationUrl, false);

        try {
            int statusCode = httpClient.executeMethod(copyMethod);

            assertEquals(statusCode, HttpStatus.SC_CREATED);
        } finally {
            copyMethod.releaseConnection();
        }

        targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview";
        PropFindMethod propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(22, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40exports/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40versions/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Email%20archiving/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Example%20Index/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Formatting%20Content/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Jira%20integration/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/News/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/RSS%20Capabilities/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Search%20Confluence%20content/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Source%20code/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Tasklist/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Thumbnail%20gallery/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Using%20Spaces/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Confluence%20Overview.txt",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Confluence%20Overview.url",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/excelspreadsheet.xls",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/harbour.jpg",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/pdfdoc.pdf",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/worddocument.doc",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }


        targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)";
        propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(6, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)/%40exports/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)/%40versions/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)/Copy%20of%20Breadcrumb%20demonstration/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)/Creating%20pages%20and%20linking%20(2).txt",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)/Creating%20pages%20and%20linking%20(2).url",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }


        targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking";
        destinationUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(3)";
        copyMethod = new CopyMethod(targetUrl, destinationUrl, false);

        try {
            int statusCode = httpClient.executeMethod(copyMethod);

            assertEquals(statusCode, HttpStatus.SC_CREATED);
        } finally {
            copyMethod.releaseConnection();
        }

        targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview";
        propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(23, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40exports/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/%40versions/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(2)/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(3)/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Email%20archiving/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Example%20Index/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Formatting%20Content/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Jira%20integration/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/News/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/RSS%20Capabilities/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Search%20Confluence%20content/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Source%20code/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Tasklist/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Thumbnail%20gallery/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Using%20Spaces/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Confluence%20Overview.txt",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Confluence%20Overview.url",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/excelspreadsheet.xls",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/harbour.jpg",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/pdfdoc.pdf",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/worddocument.doc",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }

        targetUrl = getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(3)";
        propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(6, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(3)/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(3)/%40exports/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(3)/%40versions/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(3)/Copy%20of%20Copy%20of%20Breadcrumb%20demonstration/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(3)/Creating%20pages%20and%20linking%20(3).txt",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/Creating%20pages%20and%20linking%20(3)/Creating%20pages%20and%20linking%20(3).url",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }
    }

    /**
     * WBDV-113
     * Issue tracked - http://developer.atlassian.com/jira/browse/WBDV-113
     *
     * @throws IOException
     * @throws DavException
     */
    public void testMkColGlobalCreateNewUmlautsCharacterPageAsFolders() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/%C3%84%C3%BC%C3%9F%C3%B6%C3%A4%C3%96%C3%9C";
        MkColMethod mkColMethod = new MkColMethod(targetUrl);

        try {
            int statusCode = httpClient.executeMethod(mkColMethod);
            assertEquals(statusCode, HttpStatus.SC_CREATED);
        } finally {
    		/* Always always call releaseConnection() in finally. */
            mkColMethod.releaseConnection();
        }

        targetUrl = getWebdavServletUrl() + "/Global/ds";
        PropFindMethod propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(6, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Global/ds/",
                            getWebdavServletUrl() + "/Global/ds/%40news/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                            getWebdavServletUrl() + "/Global/ds/Index/",
                            getWebdavServletUrl() + "/Global/ds/%c3%84%c3%bc%c3%9f%c3%b6%c3%a4%c3%96%c3%9c/",
                            getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }
    }

    /**
     * WBDV-108
     * Issue tracked - http://developer.atlassian.com/jira/browse/WBDV-108
     *
     * @throws IOException
     * @throws DavException
     */
    public void testMkColGlobalCreateNewScandinavianCharacterPageAsFolders() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/sv%C3%ADnakj%C3%B6t";
        MkColMethod mkColMethod = new MkColMethod(targetUrl);

        try {
            int statusCode = httpClient.executeMethod(mkColMethod);
            assertEquals(statusCode, HttpStatus.SC_CREATED);
        } finally {
    		/* Always always call releaseConnection() in finally. */
            mkColMethod.releaseConnection();
        }

        targetUrl = getWebdavServletUrl() + "/Global/ds";
        PropFindMethod propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(6, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Global/ds/",
                            getWebdavServletUrl() + "/Global/ds/%40news/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                            getWebdavServletUrl() + "/Global/ds/Index/",
                            getWebdavServletUrl() + "/Global/ds/sv%c3%adnakj%c3%b6t/",
                            getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }
    }

    /**
     * Test MKCOL command by creating new non ascii page in Global Space
     *
     * @throws IOException
     * @throws DavException
     */
    public void testMkColGlobalCreateNewNonAsciiPageAsFolders() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Global/ds/%E5%A5%BD";
        MkColMethod mkColMethod = new MkColMethod(targetUrl);

        try {
            int statusCode = httpClient.executeMethod(mkColMethod);
            assertEquals(statusCode, HttpStatus.SC_CREATED);
        } finally {
    		/* Always always call releaseConnection() in finally. */
            mkColMethod.releaseConnection();
        }

        targetUrl = getWebdavServletUrl() + "/Global/ds";
        PropFindMethod propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(6, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Global/ds/",
                            getWebdavServletUrl() + "/Global/ds/%40news/",
                            getWebdavServletUrl() + "/Global/ds/Confluence%20Overview/",
                            getWebdavServletUrl() + "/Global/ds/Index/",
                            getWebdavServletUrl() + "/Global/ds/%e5%a5%bd/",
                            getWebdavServletUrl() + "/Global/ds/Demonstration%20Space.txt",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }
    }

    // ## < TEST PERMISSIONS > ##

    /**
     * Test MKCOL command without create permission
     *
     * @throws IOException
     * @throws DavException
     */
    public void testMkColGlobalTestCreatePagePermissionAsFolders() throws IOException, DavException {
        httpClient.getState().setCredentials(
                new AuthScope(
                        getWebdavServerHostName(),
                        getWebdavServerPort(),
                        AuthScope.ANY_REALM)
                , new UsernamePasswordCredentials("sakura", "sakura")
        );

        String targetUrl = getWebdavServletUrl() + "/Global/ds/newpage";
        MkColMethod mkColMethod = new MkColMethod(targetUrl);

        try {
            int statusCode = httpClient.executeMethod(mkColMethod);
            assertEquals(statusCode, HttpStatus.SC_FORBIDDEN);
        } finally {
    		/* Always always call releaseConnection() in finally. */
            mkColMethod.releaseConnection();
        }

        httpClient.getState().setCredentials(
                new AuthScope(
                        getWebdavServerHostName(),
                        getWebdavServerPort(),
                        AuthScope.ANY_REALM
                )
                , new UsernamePasswordCredentials("admin", "admin")
        );
    }

    /**
     * Test DELETE command without delete permission
     *
     * @throws IOException
     * @throws DavException
     */
    public void testDelGlobalTestRemovePagePermissionAsFolders() throws IOException, DavException {
        httpClient.getState().setCredentials(
                new AuthScope(
                        getWebdavServerHostName(),
                        getWebdavServerPort(),
                        AuthScope.ANY_REALM)
                , new UsernamePasswordCredentials("sakura", "sakura")
        );

        String targetUrl = getWebdavServletUrl() + "/Global/ds/Index";
        DeleteMethod deleteMethod = new DeleteMethod(targetUrl);

        try {
            int statusCode = httpClient.executeMethod(deleteMethod);
            assertEquals(statusCode, HttpStatus.SC_FORBIDDEN);
        } finally {
            deleteMethod.releaseConnection();
        }

        httpClient.getState().setCredentials(
                new AuthScope(
                        getWebdavServerHostName(),
                        getWebdavServerPort(),
                        AuthScope.ANY_REALM
                )
                , new UsernamePasswordCredentials("admin", "admin")
        );
    }

    /**
     * Test MOVE command without rename permission
     *
     * @throws IOException
     * @throws DavException
     */
    public void testMoveGlobalTestRenamePagePermissionAsFolders() throws IOException, DavException {
        httpClient.getState().setCredentials(
                new AuthScope(
                        getWebdavServerHostName(),
                        getWebdavServerPort(),
                        AuthScope.ANY_REALM)
                , new UsernamePasswordCredentials("sakura", "sakura")
        );

        String targetUrl = getWebdavServletUrl() + "/Global/ds/Index";
        String destinationUrl = getWebdavServletUrl() + "/Global/ds/Index2";
        MoveMethod moveMethod = new MoveMethod(targetUrl, destinationUrl, false);

        try {
            int statusCode = httpClient.executeMethod(moveMethod);
            assertEquals(statusCode, HttpStatus.SC_FORBIDDEN);
        } finally {
            moveMethod.releaseConnection();
        }

        httpClient.getState().setCredentials(
                new AuthScope(
                        getWebdavServerHostName(),
                        getWebdavServerPort(),
                        AuthScope.ANY_REALM
                )
                , new UsernamePasswordCredentials("admin", "admin")
        );
    }


    // ------ < Personal > ----------------------------------------------------------------

    /**
     * Test MKCOL command by creating new page in Personal Space
     *
     * @throws IOException
     * @throws DavException
     */
    public void testMkColPersonalCreateNewPageAsFolders() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Personal/~admin/newpage";
        MkColMethod mkColMethod = new MkColMethod(targetUrl);

        try {
            int statusCode = httpClient.executeMethod(mkColMethod);
            assertEquals(statusCode, HttpStatus.SC_CREATED);
        } finally {
    		/* Always always call releaseConnection() in finally. */
            mkColMethod.releaseConnection();
        }

        targetUrl = getWebdavServletUrl() + "/Personal/~admin";
        PropFindMethod propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(5, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Personal/~admin/",
                            getWebdavServletUrl() + "/Personal/~admin/%40news/",
                            getWebdavServletUrl() + "/Personal/~admin/Home/",
                            getWebdavServletUrl() + "/Personal/~admin/newpage/",
                            getWebdavServletUrl() + "/Personal/~admin/admin.txt",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }
    }

    /**
     * Test DELETE command by removing page in Personal Space
     *
     * @throws IOException
     * @throws DavException
     */
    public void testDelPersonalRemovePageAsFolders() throws IOException, DavException {
        PageHelper pageHelper = getPageHelper();
        pageHelper.setSpaceKey("~admin");
        pageHelper.setTitle("newpage");

        assertTrue(pageHelper.create());

        String targetUrl = getWebdavServletUrl() + "/Personal/~admin";
        PropFindMethod propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(5, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Personal/~admin/",
                            getWebdavServletUrl() + "/Personal/~admin/%40news/",
                            getWebdavServletUrl() + "/Personal/~admin/Home/",
                            getWebdavServletUrl() + "/Personal/~admin/newpage/",
                            getWebdavServletUrl() + "/Personal/~admin/admin.txt",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }

        targetUrl = getWebdavServletUrl() + "/Personal/~admin/newpage";
        DeleteMethod deleteMethod = new DeleteMethod(targetUrl);

        try {
            httpClient.executeMethod(deleteMethod);
        } finally {
            deleteMethod.releaseConnection();
        }

        targetUrl = getWebdavServletUrl() + "/Personal/~admin";
        propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(4, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Personal/~admin/",
                            getWebdavServletUrl() + "/Personal/~admin/%40news/",
                            getWebdavServletUrl() + "/Personal/~admin/Home/",
                            getWebdavServletUrl() + "/Personal/~admin/admin.txt",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }
    }

    /**
     * Test MOVE command by renaming page in Personal Space
     *
     * @throws IOException
     * @throws DavException
     */
    public void testMovePersonalRenamePageAsFolders() throws IOException, DavException {
        PageHelper pageHelper = getPageHelper();
        pageHelper.setSpaceKey("~admin");
        pageHelper.setTitle("newpage");

        assertTrue(pageHelper.create());

        String targetUrl = getWebdavServletUrl() + "/Personal/~admin";
        PropFindMethod propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(5, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Personal/~admin/",
                            getWebdavServletUrl() + "/Personal/~admin/%40news/",
                            getWebdavServletUrl() + "/Personal/~admin/Home/",
                            getWebdavServletUrl() + "/Personal/~admin/newpage/",
                            getWebdavServletUrl() + "/Personal/~admin/admin.txt",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }

        targetUrl = getWebdavServletUrl() + "/Personal/~admin/newpage";
        String destinationUrl = getWebdavServletUrl() + "/Personal/~admin/pagenew";
        MoveMethod moveMethod = new MoveMethod(targetUrl, destinationUrl, true);

        try {
            httpClient.executeMethod(moveMethod);
        } finally {
            moveMethod.releaseConnection();
        }

        targetUrl = getWebdavServletUrl() + "/Personal/~admin";
        propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(5, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Personal/~admin/",
                            getWebdavServletUrl() + "/Personal/~admin/%40news/",
                            getWebdavServletUrl() + "/Personal/~admin/Home/",
                            getWebdavServletUrl() + "/Personal/~admin/pagenew/",
                            getWebdavServletUrl() + "/Personal/~admin/admin.txt",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }
    }

    /**
     * Test COPY command by copying page to same location in Personal Space
     *
     * @throws IOException
     * @throws DavException
     */
    public void testCopyPersonalPageAsFolder() throws IOException, DavException {
        String targetUrl = getWebdavServletUrl() + "/Personal/~admin/Home";
        String destinationUrl = getWebdavServletUrl() + "/Personal/~admin/Home%20(2)";
        CopyMethod copyMethod = new CopyMethod(targetUrl, destinationUrl, false);

        try {
            int statusCode = httpClient.executeMethod(copyMethod);

            assertEquals(statusCode, HttpStatus.SC_CREATED);
        } finally {
            copyMethod.releaseConnection();
        }

        targetUrl = getWebdavServletUrl() + "/Personal/~admin";
        PropFindMethod propFindMethod = new PropFindMethod(
                targetUrl,
                DavConstants.PROPFIND_ALL_PROP,
                DavConstants.DEPTH_1);

        try {
            MultiStatus multiStatus;
            MultiStatusResponse[] multiStatusResponses;
            List expectedMultiStatusResponseHrefs;
            List actualMultiStatusResponseHrefs;

            httpClient.executeMethod(propFindMethod);

            multiStatus = propFindMethod.getResponseBodyAsMultiStatus();
            multiStatusResponses = multiStatus.getResponses();

            assertNotNull(multiStatusResponses);
            assertEquals(5, multiStatusResponses.length);

            expectedMultiStatusResponseHrefs = Arrays.asList(
                    new String[]{
                            getWebdavServletUrl() + "/Personal/~admin/",
                            getWebdavServletUrl() + "/Personal/~admin/%40news/",
                            getWebdavServletUrl() + "/Personal/~admin/Home/",
                            getWebdavServletUrl() + "/Personal/~admin/Home%20(2)/",
                            getWebdavServletUrl() + "/Personal/~admin/admin.txt",
                    }
            );
            actualMultiStatusResponseHrefs = getMultiStatusHrefs(multiStatusResponses);

            assertEquals(expectedMultiStatusResponseHrefs.size(), actualMultiStatusResponseHrefs.size());
            assertTrue(expectedMultiStatusResponseHrefs.containsAll(actualMultiStatusResponseHrefs));
        } finally {
            /* Always always call releaseConnection() in finally. */
            propFindMethod.releaseConnection();
        }
    }

    // ## < TEST PERMISSIONS > ##

    /**
     * Test MKCOL command without create permission in Personal Space
     *
     * @throws IOException
     * @throws DavException
     */
    public void testMkColPersonalTestCreatePagePermissionAsFolders() throws IOException, DavException {
        httpClient.getState().setCredentials(
                new AuthScope(
                        getWebdavServerHostName(),
                        getWebdavServerPort(),
                        AuthScope.ANY_REALM)
                , new UsernamePasswordCredentials("sakura", "sakura")
        );

        String targetUrl = getWebdavServletUrl() + "/Personal/~admin/newpage";
        MkColMethod mkColMethod = new MkColMethod(targetUrl);

        try {
            int statusCode = httpClient.executeMethod(mkColMethod);
            assertEquals(statusCode, HttpStatus.SC_FORBIDDEN);
        } finally {
        		/* Always always call releaseConnection() in finally. */
            mkColMethod.releaseConnection();
        }

        httpClient.getState().setCredentials(
                new AuthScope(
                        getWebdavServerHostName(),
                        getWebdavServerPort(),
                        AuthScope.ANY_REALM
                )
                , new UsernamePasswordCredentials("admin", "admin")
        );
    }

    /**
     * Test DELETE command without remove permission
     *
     * @throws IOException
     * @throws DavException
     */
    public void testDelPersonalTestRemovePagePermissionAsFolders() throws IOException, DavException {
        httpClient.getState().setCredentials(
                new AuthScope(
                        getWebdavServerHostName(),
                        getWebdavServerPort(),
                        AuthScope.ANY_REALM)
                , new UsernamePasswordCredentials("sakura", "sakura")
        );

        String targetUrl = getWebdavServletUrl() + "/Personal/~admin/Home";
        DeleteMethod deleteMethod = new DeleteMethod(targetUrl);

        try {
            int statusCode = httpClient.executeMethod(deleteMethod);
            assertEquals(statusCode, HttpStatus.SC_FORBIDDEN);
        } finally {
            deleteMethod.releaseConnection();
        }

        httpClient.getState().setCredentials(
                new AuthScope(
                        getWebdavServerHostName(),
                        getWebdavServerPort(),
                        AuthScope.ANY_REALM
                )
                , new UsernamePasswordCredentials("admin", "admin")
        );
    }

    /**
     * Test MOVE command without rename permission
     *
     * @throws IOException
     * @throws DavException
     */
    public void testMovePersonalTestRenamePagePermissionAsFolders() throws IOException, DavException {
        httpClient.getState().setCredentials(
                new AuthScope(
                        getWebdavServerHostName(),
                        getWebdavServerPort(),
                        AuthScope.ANY_REALM)
                , new UsernamePasswordCredentials("sakura", "sakura")
        );

        String targetUrl = getWebdavServletUrl() + "/Personal/~admin/Home";
        String destinationUrl = getWebdavServletUrl() + "/Personal/~admin/Home2";
        MoveMethod moveMethod = new MoveMethod(targetUrl, destinationUrl, false);

        try {
            int statusCode = httpClient.executeMethod(moveMethod);
            assertEquals(HttpStatus.SC_FORBIDDEN, statusCode);
        } finally {
            moveMethod.releaseConnection();
        }

        httpClient.getState().setCredentials(
                new AuthScope(
                        getWebdavServerHostName(),
                        getWebdavServerPort(),
                        AuthScope.ANY_REALM
                )
                , new UsernamePasswordCredentials("admin", "admin")
        );
    }
}
