package com.atlassian.confluence.extra.webdav.job;


/**
 * Represents a job/task to be executed related to content.
 * Used in conjuction with {@link com.atlassian.confluence.extra.webdav.job.ContentJobQueue}.
 */
public interface ContentJob {
    /**
     * Implementations should return the number of milliseconds that must have elapsed since it was
     * created before the job is considered &quot;mature&quot; enough for execution.
     *
     * @return the minimum age for execution, in milliseconds.
     */
    long getMinimumAgeForExecution();

    /**
     * Implementations should return the time the task is created.
     *
     * @return The creation time, in milliseconds.
     * @see #getMinimumAgeForExecution()
     */
    long getCreationTime();

    /**
     * This is where implementations implement task logic (what needs to be done).
     *
     * @throws Exception ex
     *                   Implementations may throw any exceptions to indicate fatal errors.
     */
    void execute() throws Exception;
}
