package com.atlassian.confluence.extra.webdav.job.impl;

import com.atlassian.confluence.extra.webdav.job.ContentJob;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

/**
 * An implementation of {@link ContentJob} that removes content attachments.
 */
public class AttachmentRemovalJob implements ContentJob {
    /**
     * The ID of the content to remove attachments of.
     */
    private long containingPageId;

    /**
     * The name of the attachment to remove.
     */
    private String fileName;

    /**
     * When this job was created.
     */
    private long creationTime;

    /* We don't want XStream to seralize this */
    private transient AttachmentManager attachmentManager;

    /* We don't want XStream to seralize this, too */
    private transient PageManager pageManager;

    public AttachmentRemovalJob(
            @ComponentImport PageManager pageManager,
            @ComponentImport AttachmentManager attachmentManager,
            long containingPageId,
            String fileName) {
        setContainingPageId(containingPageId);
        setFileName(fileName);
        setAttachmentManager(attachmentManager);
        setPageManager(pageManager);
        setCreationTime(System.currentTimeMillis());
    }

    public long getMinimumAgeForExecution() {
        return 15000; /* If a WebDAV client asks for a page removal, this task will be queue and it must be of age older than 30 seconds to be executed */
    }

    public long getContainingPageId() {
        return containingPageId;
    }

    public void setContainingPageId(long containingPageId) {
        this.containingPageId = containingPageId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(long creationTime) {
        this.creationTime = creationTime;
    }

    public void setAttachmentManager(AttachmentManager attachmentManager) {
        this.attachmentManager = attachmentManager;
    }

    public void setPageManager(PageManager pageManager) {
        this.pageManager = pageManager;
    }

    public void execute() throws Exception {
        Page page = pageManager.getPage(getContainingPageId());

        if (null != page) {
            Attachment attachment = attachmentManager.getAttachment(page, getFileName());
            if (null != attachment)
                attachmentManager.removeAttachmentFromServer(attachment);
        }
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AttachmentRemovalJob that = (AttachmentRemovalJob) o;

        if (containingPageId != that.containingPageId) return false;
        if (fileName != null ? !fileName.equals(that.fileName) : that.fileName != null) return false;

        return true;
    }

    public int hashCode() {
        int result;
        result = (int) (containingPageId ^ (containingPageId >>> 32));
        result = 31 * result + (fileName != null ? fileName.hashCode() : 0);
        return result;
    }

    public String toString() {
        return new StringBuffer(getClass().toString())
                .append(" [ Page ID: ").append(containingPageId).append("; Attachment File Name: ").append(fileName).append(" ]")
                .toString();
    }
}
