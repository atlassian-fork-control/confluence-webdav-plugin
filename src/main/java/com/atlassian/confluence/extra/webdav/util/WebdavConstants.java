package com.atlassian.confluence.extra.webdav.util;

import com.atlassian.confluence.extra.webdav.resource.BlogPostsResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.PageExportsResourceImpl;
import com.atlassian.confluence.extra.webdav.resource.PageVersionsResourceImpl;
import org.apache.jackrabbit.webdav.DavConstants;

import java.util.Arrays;
import java.util.List;

public interface WebdavConstants extends DavConstants {
    String HEADER_USER_AGENT = "User-Agent";

    String HEADER_HOST = "Host";

    String HEADER_CONNECTION = "Connection";

    /**
     * Special directory names in the plugin. Currently, they are:
     * <ul>
     * <li>@news &mdash; See {@link BlogPostsResourceImpl}</li>
     * <li>@exports &mdash; See {@link com.atlassian.confluence.extra.webdav.resource.PageExportsResourceImpl}</li>
     * <li>@versions &mdash; See {@link PageVersionsResourceImpl}</li>
     * </ul>
     */
    List<String> SPECIAL_DIRECTORY_NAMES = Arrays.asList(
            BlogPostsResourceImpl.DISPLAY_NAME,
            PageExportsResourceImpl.DISPLAY_NAME,
            PageVersionsResourceImpl.DISPLAY_NAME
    );
}
