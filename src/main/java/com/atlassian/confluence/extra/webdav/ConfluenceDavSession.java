package com.atlassian.confluence.extra.webdav;

import com.atlassian.confluence.extra.webdav.util.UserAgentUtil;
import org.apache.jackrabbit.webdav.DavSession;
import org.apache.jackrabbit.webdav.lock.LockManager;
import org.apache.jackrabbit.webdav.lock.SerializableLockManager;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Represents a user WebDAV session.
 *
 * @author weiching.cher
 */
public class ConfluenceDavSession implements DavSession, Serializable {
    private String userName;

    private ResourceStates resourceStates;

    private Set<String> lockTokens;

    private LockManager lockManager;

    private long lastActivityTimestamp;

    private String userAgent;

    private boolean currentlyBeingUsed;

    public ConfluenceDavSession(String userName) {
        this.userName = userName;
        resourceStates = new ResourceStates();
        lockTokens = new HashSet<>();
        lockManager = new SerializableLockManager();
        updateActivityTimestamp();
    }

    protected ConfluenceDavSession() {
        /* For Java IO deserialization */
    }

    public String getUserName() {
        return userName;
    }

    public ResourceStates getResourceStates() {
        return resourceStates;
    }

    public LockManager getLockManager() {
        return lockManager;
    }

    public void updateActivityTimestamp() {
        lastActivityTimestamp = System.currentTimeMillis();
    }

    public long getLastActivityTimestamp() {
        return lastActivityTimestamp;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public boolean isCurrentlyBeingUsed() {
        return currentlyBeingUsed;
    }

    public void setCurrentlyBeingUsed(boolean currentlyBeingUsed) {
        this.currentlyBeingUsed = currentlyBeingUsed;
    }

    public boolean isClientFinder() {
        return UserAgentUtil.isOsxFinder(userAgent);
    }

    /**
     * No-op.
     */
    public void addReference(Object o) {

    }

    /**
     * No-op.
     */
    public void removeReference(Object o) {

    }

    public void addLockToken(String s) {
        lockTokens.add(s);
    }

    public String[] getLockTokens() {
        return lockTokens.toArray(new String[0]);
    }

    public void removeLockToken(String s) {
        lockTokens.remove(s);
    }
}
