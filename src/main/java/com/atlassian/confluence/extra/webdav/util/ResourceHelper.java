package com.atlassian.confluence.extra.webdav.util;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.renderer.util.FileTypeUtil;
import com.atlassian.user.User;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.jackrabbit.webdav.io.InputContext;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;

public class ResourceHelper {
    public static File getInputContextContentAsFile(final InputContext inputContext) throws IOException {
        final File tempFile = File.createTempFile("webdav", null);
        InputStream in = null;
        OutputStream out = null;

        try {
            in = inputContext.getInputStream();
            out = new BufferedOutputStream(new FileOutputStream(tempFile));

            IOUtils.copy(in, out);

            return tempFile;
        } finally {
            IOUtils.closeQuietly(out);
            IOUtils.closeQuietly(in);
        }
    }

    public static void addOrUpdateAttachment(AttachmentManager attachmentManager, ContentEntityObject ceo, String fileName, InputContext inputContext) throws IOException, CloneNotSupportedException {
        Attachment attachment = attachmentManager.getAttachment(ceo, fileName);
        Attachment previousVersionOfAttachment = null;
        File inputContentFile = null;
        InputStream inputContentStream = null;

        if (null == attachment) {
            attachment = new Attachment();
        } else {
            previousVersionOfAttachment = (Attachment) attachment.clone();
        }

        try {
            inputContentFile = getInputContextContentAsFile(inputContext);


            if ((!fileName.startsWith("._") && inputContentFile.length() > 0) || fileName.startsWith("._") || !attachment.isPersistent()) {
                User user = AuthenticatedUserThreadLocal.getUser();

                attachment.setFileName(fileName);
                attachment.setFileSize(inputContentFile.length());
                attachment.setCreatorName(user.getName());
                attachment.setCreationDate(new Date());
                attachment.setLastModifierName(user.getName());
                attachment.setLastModificationDate(attachment.getCreationDate());
                attachment.setContainer(ceo);
                // MIME type detection now depends entirely on http://java.sun.com/j2ee/sdk_1.3/techdocs/api/javax/activation/MimetypesFileTypeMap.html#getContentType(java.lang.String)
                attachment.setContentType(StringUtils.isBlank(inputContext.getContentType()) ? FileTypeUtil.getContentType(fileName) : inputContext.getContentType());

                ceo.addAttachment(attachment);

                inputContentStream = new BufferedInputStream(new FileInputStream(inputContentFile));
                attachmentManager.saveAttachment(attachment, previousVersionOfAttachment, inputContentStream);
            }
        } finally {
            IOUtils.closeQuietly(inputContentStream);
            if (null != inputContentFile)
                inputContentFile.delete();
        }
    }
}
