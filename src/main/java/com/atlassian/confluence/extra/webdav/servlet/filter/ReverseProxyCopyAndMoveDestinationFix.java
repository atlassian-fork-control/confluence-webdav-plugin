package com.atlassian.confluence.extra.webdav.servlet.filter;

import com.atlassian.confluence.extra.webdav.util.WebdavConstants;
import org.apache.commons.lang.StringUtils;
import org.apache.jackrabbit.webdav.DavMethods;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * This is really a hack that rewrites the "Destination" header in the MOVE and COPY WebDAV HTTP requests
 * coming from reverse proxies.
 */
public class ReverseProxyCopyAndMoveDestinationFix extends AbstractPrefixAwareFilter {
    private static final Pattern GLOBAL_RESOURCE_PATTERN = Pattern.compile("/(\\bGlobal\\b|\\bPersonal\\b)");

    private boolean isCopyOrMoveMethod(HttpServletRequest httpServletRequest) {
        return StringUtils.equals(httpServletRequest.getMethod(), DavMethods.METHOD_MOVE)
                || StringUtils.equals(httpServletRequest.getMethod(), DavMethods.METHOD_COPY);
    }

    private HttpServletRequest rewriteDestinationHeader(HttpServletRequest httpServletRequest) {
        String destination = httpServletRequest.getHeader(WebdavConstants.HEADER_DESTINATION);

        if (StringUtils.isNotBlank(destination)) {
            Matcher globalResourcePatternMatcher = GLOBAL_RESOURCE_PATTERN.matcher(destination);

            if (globalResourcePatternMatcher.find()) {
                String rewrittenDestination = new StringBuffer()
                        .append(httpServletRequest.getScheme()).append("://")
                        .append(httpServletRequest.getHeader(WebdavConstants.HEADER_HOST))
                        .append(httpServletRequest.getContextPath())
                        .append(getPrefix())
                        .append(destination.substring(globalResourcePatternMatcher.start()))
                        .toString();

                return new HttpServletRequestWrapperWithModifiedDestinationHeader(
                        httpServletRequest, rewrittenDestination
                );
            }
        }

        return httpServletRequest;
    }

    public void doFilter(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws IOException, ServletException {
        filterChain.doFilter(
                isCopyOrMoveMethod(httpServletRequest)
                        ? rewriteDestinationHeader(httpServletRequest)
                        : httpServletRequest,
                httpServletResponse);
    }

    private static class HttpServletRequestWrapperWithModifiedDestinationHeader extends HttpServletRequestWrapper {
        private final String destinationHeaderValue;

        public HttpServletRequestWrapperWithModifiedDestinationHeader(
                HttpServletRequest httpServletRequest,
                String destinationHeaderValue) {
            super(httpServletRequest);
            this.destinationHeaderValue = destinationHeaderValue;
        }

        public String getHeader(String name) {
            if (StringUtils.equals(WebdavConstants.HEADER_DESTINATION, name))
                return destinationHeaderValue;
            else
                return super.getHeader(name);
        }
    }
}
