package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.core.VersionHistorySummary;
import com.atlassian.confluence.extra.webdav.ConfluenceDavSession;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.lock.LockManager;

import java.io.UnsupportedEncodingException;

public abstract class AbstractVersionContentResource extends AbstractTextContentResource {
    public static final String DISPLAY_NAME_PREFIX = "Version ";

    public static final String DISPLAY_NAME_SUFFIX = ".txt";

    private final PageManager pageManager;

    protected final int versionNumber;

    private AbstractPage versionedPage;

    public AbstractVersionContentResource(
            DavResourceLocator davResourceLocator,
            DavResourceFactory davResourceFactory,
            LockManager lockManager,
            ConfluenceDavSession davSession,
            @ComponentImport SettingsManager settingsManager,
            @ComponentImport PageManager pageManager,
            int versionNumber) {
        super(davResourceLocator, davResourceFactory, lockManager, davSession, settingsManager);
        this.pageManager = pageManager;
        this.versionNumber = versionNumber;
    }

    protected PageManager getPageManager() {
        return pageManager;
    }

    public int getVersionNumber() {
        return versionNumber;
    }

    public abstract AbstractPage getAbstractPage();

    public AbstractPage getVersionedPage() {
        if (null == versionedPage)
            versionedPage = pageManager.getPageByVersion(getAbstractPage(), versionNumber);
        return versionedPage;
    }

    protected byte[] getTextContentAsBytes(String encoding) throws UnsupportedEncodingException {
        return getVersionedPage().getBodyContent().getBody().getBytes(encoding);
    }

    public long getModificationTime() {
        return getCreationtTime(); /* Because it doesn't make sense that a version history has different creation and modification dates. You can't edit history. */
    }

    protected long getCreationtTime() {
        return getVersionedPage().getCreationDate().getTime();
    }

    private boolean versionExists() {
        for (VersionHistorySummary versionHistorySummary : pageManager.getVersionHistorySummaries(getAbstractPage())) {
            if (versionHistorySummary.getVersion() == versionNumber)
                return true;
        }
        return false;
    }

    public boolean exists() {
        return super.exists()
                && versionExists()
                && !((ConfluenceDavSession) getSession()).getResourceStates().isContentVersionTextHidden(getAbstractPage(), DISPLAY_NAME_PREFIX + versionNumber + DISPLAY_NAME_SUFFIX);
    }

    public String getDisplayName() {
        return new StringBuffer(DISPLAY_NAME_PREFIX).append(versionNumber).append(DISPLAY_NAME_SUFFIX).toString();
    }
}
