package com.atlassian.confluence.extra.webdav;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.dom4j.Element;

import javax.annotation.Nonnull;

public class DefaultDavResourceFactoryModuleDescriptor extends AbstractModuleDescriptor<DavResourceFactory> implements DavResourceFactoryModuleDescriptor {
    private String workspaceName;

    public DefaultDavResourceFactoryModuleDescriptor(@ComponentImport ModuleFactory moduleFactory) {
        super(moduleFactory);
    }

    @Override
    public void init(@Nonnull final Plugin plugin, @Nonnull Element element) throws PluginParseException {
        super.init(plugin, element);
        workspaceName = element.attributeValue("workspace");
    }

    public DavResourceFactory getModule() {
        return moduleFactory.createModule(moduleClassName, this);
    }

    /**
     * {@inheritDoc}
     */
    public String getWorkspaceName() {
        return workspaceName;
    }
}
