package com.atlassian.confluence.extra.webdav.servlet.filter;

import org.apache.commons.lang.StringUtils;

import javax.servlet.FilterConfig;
import javax.servlet.ServletException;

/**
 * A Servlet filter that knows about the prefix (relative to context root) to the workspace
 * of the WebDAV servlet (/plugins/servlet/confluence/default, by default).
 */
public abstract class AbstractPrefixAwareFilter extends AbstractHttpFilter {
    private static final String INIT_PARAM_PREFIX = "resource-prefix";

    private String prefix;

    public void init(FilterConfig filterConfig) throws ServletException {
        super.init(filterConfig);
        prefix = StringUtils.defaultString(
                filterConfig.getInitParameter(INIT_PARAM_PREFIX),
                "/plugins/servlet/confluence/default"
        );
    }

    protected String getPrefix() {
        return prefix;
    }

    public void destroy() {
        prefix = null;
    }
}
