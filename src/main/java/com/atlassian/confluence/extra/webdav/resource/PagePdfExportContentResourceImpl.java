package com.atlassian.confluence.extra.webdav.resource;

import com.atlassian.confluence.extra.webdav.ConfluenceDavSession;
import com.atlassian.confluence.importexport.DefaultExportContext;
import com.atlassian.confluence.importexport.ImportExportException;
import com.atlassian.confluence.importexport.ImportExportManager;
import com.atlassian.confluence.importexport.impl.ExportScope;
import com.atlassian.confluence.pages.ContentNode;
import com.atlassian.confluence.pages.ContentTree;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.core.util.ProgressMeter;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.spring.container.ComponentNotFoundException;
import com.atlassian.spring.container.ContainerManager;
import com.atlassian.user.User;
import com.opensymphony.webwork.ServletActionContext;
import org.apache.jackrabbit.webdav.DavResourceFactory;
import org.apache.jackrabbit.webdav.DavResourceLocator;
import org.apache.jackrabbit.webdav.lock.LockManager;
import org.apache.log4j.Logger;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Represents the PDF export of page in a page's <tt>&amp;#064;exports</tt> directory.
 *
 * @author weiching.cher
 */
public class PagePdfExportContentResourceImpl extends AbstractPageExportContentResource {

    public static final String CONTENT_TYPE = "application/pdf";
    public static final String DISPLAY_NAME_SUFFIX = ".pdf";
    private static final Logger LOGGER = Logger.getLogger(PagePdfExportContentResourceImpl.class);
    private final ImportExportManager importExportManager;

    public PagePdfExportContentResourceImpl(
            final DavResourceLocator davResourceLocator,
            final DavResourceFactory davResourceFactory,
            final LockManager lockManager,
            final ConfluenceDavSession davSession,
            @ComponentImport final ImportExportManager importExportManager,
            @ComponentImport final PageManager pageManager,
            final String spaceKey,
            final String pageTitle) {
        super(davResourceLocator, davResourceFactory, lockManager, davSession, pageManager, spaceKey, pageTitle);
        this.importExportManager = importExportManager;
    }

    public boolean exists() {
        return super.exists()
                && !((ConfluenceDavSession) getSession()).getResourceStates().isContentPdfExportHidden(getPage());
    }

    protected String getExportSuffix() {
        return DISPLAY_NAME_SUFFIX;
    }

    private Object getPdfExporterService() {
        try {
            return ContainerManager.getComponent("flyingPdfExporterService");
        } catch (ComponentNotFoundException cnfe) {
            LOGGER.debug("Unable to find PdfExporterService. Falling back to ImportExportManager for the export.", cnfe);
            return null;
        }
    }

    private File generatePdfExportFromPdfExporterService(Object pdfExporterService)
            throws NoSuchMethodException,
            IllegalAccessException,
            InvocationTargetException

    {
        Method exportMethod = pdfExporterService.getClass().getMethod("createPdfForPage", User.class, Page.class, String.class);
        return (File) exportMethod.invoke(
                pdfExporterService,
                AuthenticatedUserThreadLocal.get(),
                getPage(),
                ServletActionContext.getRequest().getContextPath()
        );
    }

    private File generatePdfExportFromImportExportManager() throws ImportExportException {
        final Page thisPage = getPage();

        final DefaultExportContext context = new DefaultExportContext();
        context.setExportScope(ExportScope.PAGE);
        context.setType(ImportExportManager.TYPE_PDF);
        context.addWorkingEntity(thisPage);

        context.setExportComments(false);
        context.setExportAttachments(false);
        context.setExportHierarchy(false);
        context.setUser(AuthenticatedUserThreadLocal.get());

        final ContentTree contentTree = new ContentTree();
        contentTree.addRootNode(new ContentNode(thisPage));
        context.setContentTree(contentTree);

        return new File(importExportManager.exportAs(context, new ProgressMeter()));
    }

    protected InputStream getContentInternal() {
        try {
            /* Since Confluence 3.0, PDF export is no longer supported by ImportExportManager. The responsibility
             * of that has become FlyingPDF's. So we need to see if the component that provides the PDF export
             * service is available. If it does, we'll use that. If not, we're probably running in Confluence 2.10
             * (which supports PDF exports via ImportExportManager)
             */
            Object pdfExporterService = getPdfExporterService();

            return new BufferedInputStream(
                    new FileInputStream(
                            null != pdfExporterService
                                    ? generatePdfExportFromPdfExporterService(pdfExporterService)
                                    : generatePdfExportFromImportExportManager()
                    )
            );
        } catch (Exception e) {
            LOGGER.error("Error exporting " + getPage() + " as PDF. Returning InputStream with one byte", e);
            return new ByteArrayInputStream(new byte[1]);
        }
    }

    protected String getContentType() {
        return CONTENT_TYPE;
    }

    public String getDisplayName() {
        return getPage().getTitle() + DISPLAY_NAME_SUFFIX;
    }
}
