package com.atlassian.confluence.extra.webdav.servlet.filter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * A filter that only filters HttpServletRequest/HttpServletResponse pairs. Otherwise, it simply
 * passes the control to the next guy in the chain.
 */
public abstract class AbstractHttpFilter implements Filter {
    public void init(FilterConfig filterConfig) throws ServletException {
        /* Does nothing */
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        if (servletRequest instanceof HttpServletRequest) {
            HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
            HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;

            if (handles(httpServletRequest, httpServletResponse)) {

                doFilter(
                        (HttpServletRequest) servletRequest,
                        (HttpServletResponse) servletResponse,
                        filterChain
                );
                return;
            }
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }

    public abstract void doFilter(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain)
            throws IOException, ServletException;

    protected boolean handles(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        return true;
    }

    public void destroy() {
        /* Does nothing */
    }
}
