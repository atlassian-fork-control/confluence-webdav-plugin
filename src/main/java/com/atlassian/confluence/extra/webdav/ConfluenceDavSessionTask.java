package com.atlassian.confluence.extra.webdav;

/**
 * Represents a task that can be executed on sessions maintained by a
 * {@link com.atlassian.confluence.extra.webdav.ConfluenceDavSessionStore}.
 */
public interface ConfluenceDavSessionTask {
    /**
     * Implementations may do things to the {@link com.atlassian.confluence.extra.webdav.ConfluenceDavSession}
     * instances passed to it.
     *
     * @param confluenceDavSession The {@link com.atlassian.confluence.extra.webdav.ConfluenceDavSession} to do stuff on.
     */
    public void execute(ConfluenceDavSession confluenceDavSession);
}
