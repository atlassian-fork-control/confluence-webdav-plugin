package org.apache.jackrabbit.webdav.lock;

import org.apache.jackrabbit.webdav.DavConstants;

import java.io.Serializable;
import java.util.UUID;

/**
 * Copied entirely from {@link org.apache.jackrabbit.webdav.lock.DefaultActiveLock}with slight modifications to
 * support serialization.
 */
///CLOVER:OFF
public class SerializableActiveLock extends AbstractActiveLock implements Serializable {
    private String token = DavConstants.OPAQUE_LOCK_TOKEN_PREFIX + UUID.randomUUID().toString();
    private String owner;
    private boolean isDeep = true; // deep by default
    private long expirationTime = DavConstants.INFINITE_TIMEOUT; // never expires by default;

    /**
     * Create a new <code>DefaultActiveLock</code> with default values.
     */
    public SerializableActiveLock() {
    }

    /**
     * Create a new lock
     *
     * @param lockInfo lock info
     * @throws IllegalArgumentException if either scope or type is invalid.
     */
    public SerializableActiveLock(LockInfo lockInfo) {
        if (lockInfo != null) {
            if (!(Type.WRITE.equals(lockInfo.getType()) && Scope.EXCLUSIVE.equals(lockInfo.getScope()))) {
                throw new IllegalArgumentException("Only 'exclusive write' lock is allowed scope/type pair.");
            }
            owner = lockInfo.getOwner();
            isDeep = lockInfo.isDeep();
            setTimeout(lockInfo.getTimeout());
        }
    }

    /**
     * @see ActiveLock#isLockedByToken(String)
     */
    public boolean isLockedByToken(String lockToken) {
        return (token != null) && token.equals(lockToken);
    }

    /**
     * @see ActiveLock#isExpired()
     */
    public boolean isExpired() {
        return System.currentTimeMillis() > expirationTime;
    }

    /**
     * @see ActiveLock#getToken()
     */
    public String getToken() {
        return token;
    }

    /**
     * @see ActiveLock#getOwner()
     */
    public String getOwner() {
        return owner;
    }

    /**
     * @see ActiveLock#setOwner(String)
     */
    public void setOwner(String owner) {
        this.owner = owner;
    }

    /**
     * @see ActiveLock#getTimeout()
     */
    public long getTimeout() {
        return expirationTime - System.currentTimeMillis();
    }

    /**
     * @see ActiveLock#setTimeout(long)
     */
    public void setTimeout(long timeout) {
        if (timeout > 0) {
            expirationTime = System.currentTimeMillis() + timeout;
        }
    }

    /**
     * @see ActiveLock#isDeep()
     */
    public boolean isDeep() {
        return isDeep;
    }

    /**
     * @see ActiveLock#setIsDeep(boolean)
     */
    public void setIsDeep(boolean isDeep) {
        this.isDeep = isDeep;
    }

    /**
     * This is always a write lock.
     *
     * @return the lock type
     * @see Type#WRITE
     */
    public Type getType() {
        return Type.WRITE;
    }

    /**
     * This is always an exclusive lock.
     *
     * @return the lock scope.
     * @see Scope#EXCLUSIVE
     */
    public Scope getScope() {
        return Scope.EXCLUSIVE;
    }
}
///CLOVER:ON
